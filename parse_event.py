
import json

import requests
import settings
from bs4 import BeautifulSoup as bs
import re
from threading import Timer


from selenium import webdriver
from selenium.webdriver import FirefoxOptions
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.select import Select
from io import BytesIO
import datetime
from handlers import db_events_data, db_add_organizer, db_add_location, replace_text_to_parse


class ParseEvent:
    """
    Парсинг сайта мероприятий
    Функции скриншота:
            *- парсинг мероприятий на неделю
            *- парсинг мероприятий на сегодня
            *- парсинг мероприятий с сайта на любую дату
            *- формирование картинки (скрин)

    Функции редактирования информации на сайте
            *- авторизация на сайте
            *- отправка календаря из кнопок
            *- выборка мероприятий, для создания кнопки Редактировать
            *- добавление мероприятия
            *- извлечение списков организаторов и мест проведения
            *- редактирование мероприятия
    """
    """Функции редакора событий
            *- спарсить все мероприятия выбранного дня
            *- вывести в виде сообщений названия и время с кнопкой Подробнее и Редактировать
            *- кнопка Подробнее выдает всплывающее окно с доп инфой мероприятия
            *- кнопка Редактировать переходит в режим сценария с редактированием всей инфы
            *- в режиме редактирования появляется кнопка Сохранить, происходит отправка инфы на сервер
            """

    def __init__(self):
        self.respons_auth = ''
        self.session = requests.Session()
        self.token = ''
        # self.events = []


        self.pattern_id = r'<h5 class="modal-title" id="intervention\S(\d+)\Slabel">'

    def auth_site(self):
        """
            функция выполняет авторизацию пользователя на сайте и возвращает сессию, для возможности работы от пользователя
        :return:
        """
        url = 'https://egov.astrobl.ru/services/tech-events'
        url_form = 'https://egov.astrobl.ru/enter'
        user_agent_val = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36'

        # Создаем сессию и указываем ему наш user-agent

        response = self.session.get(url, headers={
            'User-Agent': user_agent_val
        }, verify=False)

        if response.status_code == 200:

            soup = bs(response.text, "html.parser")
            self.token = soup.find('meta', {'name': "csrf-token"})['content'] #content = "g4qjK9KqC4gdW6RAzxytpfFBNIlAlvgccfaZkxwg"

            # Указываем referer. Иногда , если не указать , то приводит к ошибкам.
            self.session.headers.update({'Referer': url})

            # Хотя , мы ранее указывали наш user-agent и запрос удачно прошел и вернул
            # нам нужный ответ, но user-agent изменился на тот , который был
            # по умолчанию. И поэтому мы обновляем его.
            self.session.headers.update({'User-Agent': user_agent_val})

            # Получаем значение _xsrf из cookies
            # _xsrf = self.session.cookies.get('XSRF-TOKEN', domain="egov.astrobl.ru")

            # Осуществляем вход с помощью метода POST с указанием необходимых данных
            post_request = self.session.post(url_form, {
                'login': settings.LOGIN,
                'password': settings.PASSWORD,
                '_token': self.token,
            })

            # print(post_request.text)
            # # Вход успешно воспроизведен и мы сохраняем страницу в html файл
            # with open("hh_success.html", "w", encoding="utf-8") as f:
            #     f.write(post_request.text)
            # self.respons_auth = post_request

    def request_create_events(self, data) -> requests:
        """
            функция отправляет данные для создания нового события на сервер через post запрос
        :param data: словарь с данными для отправки
        :return:
        """
        self.auth_site()
        # создание нового мероприятия
        url_referer = 'https://egov.astrobl.ru/interventions/create' #'https://egov.astrobl.ru/interventions/create' #https://egov.astrobl.ru/interventions
        # user_agent_val = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36'
        # self.session.headers.update({'User-Agent': user_agent_val})
        url_create_data = 'https://egov.astrobl.ru/interventions' #'https://egov.astrobl.ru/interventions/create' #https://egov.astrobl.ru/interventions
        # url_select_data = 'https://egov.astrobl.ru/interventions/859' # когда редактирование, то в конце номер мероприятия

        # Указываем referer. Иногда , если не указать , то приводит к ошибкам.
        self.session.headers.update({'Referer': url_referer})

        hour = data.hour.replace("✓", '')
        minute = data.minute.replace("✓", '')

        post_create_event = self.session.post(url_create_data, {
            'workflow_status': 'publish',  #статус Опубликовано - publish
            '_token': self.token,
            # '_method': 'PATCH',   # когда редактирование, добавляется такой параметр
            'title': data.name,
            'resource': data.organizer, #Организатор
            # 'intervention_status': 'open',    # когда редактирование, добавляется такой параметр
            'place_id': data.location,  #место
            'place_other': data.location_custom if data.location_custom else None, #место иное
            'intervention_datetime': f'{data.date} {hour}:{minute}', #17.08.2023 23:00
            'phone': None,
            'format': data.format,#'online'
            'platform': data.platform if data.platform else None, #'icep'
            'quantity_points': None,
            'description_points': None,
            'document': None,
            'select_type_intramural': None,
            'attachments_materials[]': None, # если нужно реализовать добавление файлов, нужно читать про enctype="multipart/form-data"
            'delete_attachments_materials[]': None,
            'video[0][link]': data.link if data.link else None, #'link2'
            'comments': data.comment if data.comment else None, #'comment2'
        }   )

        # print(post_create_event)
        # print(post_create_event.content)
        # with open("hh_create.html", "w", encoding="utf-8") as f:
        #     f.write(str(post_create_event.content))

        return post_create_event



    def request_edit_events(self, data) -> requests:
        """
            функция отправляет данные для редактирования события на сервер через post запрос
        :param data: словарь с данными для отправки
        :return:
        """
        self.auth_site()
        # редактирование мероприятия

        url_referer = 'https://egov.astrobl.ru/interventions/'
        # user_agent_val = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36'
        # self.session.headers.update({'User-Agent': user_agent_val})
        url_edit_data = f'https://egov.astrobl.ru/interventions/{data.event_id}'

        # Указываем referer. Иногда , если не указать , то приводит к ошибкам.
        self.session.headers.update({'Referer': url_referer})

        hour = data.hour.replace("✓", '')
        minute = data.minute.replace("✓", '')

        post_edit_event = self.session.post(url_edit_data, {
            'workflow_status': 'publish',  #статус Опубликовано - publish
            '_token': self.token,
            '_method': 'PATCH',   # когда редактирование, добавляется такой параметр
            'title': data.name,
            'resource': data.organizer, #Организатор
            'intervention_status': 'open',    # когда редактирование, добавляется такой параметр
            'place_id': data.location,  #место
            'place_other': data.location_custom if data.location_custom else None, #место иное
            'intervention_datetime': f'{data.date} {hour}:{minute}', #17.08.2023 23:00
            'phone': None,
            'format': data.format,#'online'
            'platform': data.platform if data.platform else None, #'icep'
            'quantity_points': None,
            'description_points': None,
            'document': None,
            'select_type_intramural': None,
            'attachments_materials[]': None, # если нужно реализовать добавление файлов, нужно читать про enctype="multipart/form-data"
            'delete_attachments_materials[]': None,
            'video[0][link]': data.link if data.link else None, #'link2'
            'comments': data.comment if data.comment else None, #'comment2'
        }   )

        return post_edit_event

    def request_get_organizer_location(self) -> requests:
        """
            функция парсит с сайта локации и организаторов, сокращает названия и сохраняет в базе полное и сокращенное название
        """
        self.auth_site()
        # парсинг всех возможных организаторов и мест проведения
        url = 'https://egov.astrobl.ru/interventions/create' #'https://egov.astrobl.ru/interventions/create' #https://egov.astrobl.ru/interventions

        respons = self.session.get(url)

        if respons.status_code == 200:
            organizer_data = []
            location_data = []
            soup = bs(respons.text, "html.parser")
            organizer_soup_cont = soup.find('select', {'id': 'resource'})
            organizer_soup = organizer_soup_cont.findAll('option')

            if organizer_soup:
                for org in organizer_soup:
                    if org.attrs['value']:
                        text = replace_text_to_parse(org.text, 'organizer')
                        organizer_data.append((org.attrs['value'], text, org.text))
                db_add_organizer(organizer_data)

            location_soup_cont = soup.find('select', {'id': 'place_id'})
            location_soup = location_soup_cont.findAll('option')
            if location_soup:
                for loc in location_soup:
                    if loc.attrs['value']:
                        text = replace_text_to_parse(loc.text, 'location')
                        location_data.append((loc.attrs['value'], text, loc.text))
                db_add_location(location_data)
        t = Timer(604800, self.request_get_organizer_location)  # неделя 604800 сек
        t.start()
        return True

    def request_get_events(self, date_event: str) -> requests:
        """
            функция отправялет запрос на сайт с датой, на которую нужно получить список событий и возвращает
            страницу сайта для дальнейшего парсинга
        """
        # запрос мероприятий на дату
        url_select_data = 'https://egov.astrobl.ru/select_date'
        post_select_data = self.session.post(url_select_data, {
            '_token': self.token,
            'value': date_event
        })
        return post_select_data

    def parse_events(self, date_event:  str):
        """
            Функция возвращает список словарей с информацией о мероприятиях на дату
             [{'timesec': '', 'id': '', 'data': {'name': '', 'organizer': '', 'location': '',
        #                                 'time': '', 'format': '', 'vcs': '', 'comment': ''}}, ...... ]
        :param date_event:
        :return:
        """
        events = []
        self.auth_site()
        event_data, data = {}, {}
        request_events_data = self.request_get_events(date_event)
        if request_events_data.status_code == 200:
            soup = bs(request_events_data.text, "html.parser")
            day_soup = soup.find('div', {'class': 'width-15'})
            events_soup = day_soup.findAll('div', {'class': 'modal'})
            if events_soup:
                for event in events_soup:
                    try:
                        """ 
                        в случае возникновения ошибки, возникающей из-за отсутствия некоторых элементов разметки в мероприятии, 
                        код просто пропустакает токое мероприятие и переходим к следующему.
                        (Такие карточки мероприятий, с отсутствующей разметкой возникают из-за "сбойной" заявки на сайте,
                        которые создаются из-за возникновения ошибок во время создания мероприятия через сайт)
                        """
                        event_data['id'] = re.search(self.pattern_id, str(event))[1]

                        # timesec = f'{date_event} '+ event.find('span', {'class': 'doc_item_date'}).text[-5:]
                        # event_data['timesec'] = datetime.datetime.strptime(timesec, '%Y-%m-%d %H:%M').timestamp()

                        data['name'] = event.find('h5', {'class': 'modal-title'}).text
                        data['organizer'] = (event.find('i', {'class': 'las la-university'}).parent.text).replace('Организатор: ', '')
                        data['location'] = event.find('i', {'class': 'las la-map-marker'}).parent.text.lstrip().replace('Место проведения: ', '')
                        data['time'] = event.find('span', {'class': 'doc_item_date'}).text
                        data['format'] = event.find('span', {'class': 'doc_item_numb'}).b.text
                        if data['format'] == "ОНЛАЙН":
                            if event.find('i', {'class': 'las la-video'}):
                                data['vcs'] = event.find('i', {'class': 'las la-video'}).parent.a.text
                            else:
                                data['vcs'] = ''
                        else:
                            data['vcs'] = ''
                        if event.find(string=re.compile('Комментарий:')):
                            data['comment'] = event.find(string=re.compile('Комментарий:')).parent.text.replace('Комментарий:', '')
                        else:
                            data['comment'] = ''

                        event_data['data'] = data.copy()

                        events.append(event_data.copy())
                    except Exception as err:
                        continue
                db_events_data(events)  # запись в БД
            else:
                return 'NO', f'Нет ни одного события на {date_event}'
        else:
            return 'Err', 'Сайт недоступен'

        return 'OK', events


    def screen(self, period): # : {}
        """
        функция находит нужный html элемент на странице и делает скриншот. Возвращает картинку в байтах,
        для отправки в чат пользователю
        """
        try:
            param_diapazon = period['period']
            date_event = period['data']
            options = FirefoxOptions()
            options.headless = True
            # driver = webdriver.Firefox(options=options)
            driver = webdriver.Remote("http://selenium:4444/wd/hub", options=options)
            # указываем большой размер окна иначе возникает ошибка как будто нужный элемент (дата) перекрыт и не виден
            driver.set_window_size(1920, 3000)
            driver.get('https://egov.astrobl.ru/services/tech-events') # #http://almetpt.ru/2020/site/schedulegroups/0/1/2020-03-17

            # поиск окна куки
            cookies_elem = driver.find_element(By.XPATH, '//div[@id="cookies"]//a')
            if cookies_elem:
                cookies_elem.click() #нажатие принять куки

            if param_diapazon == 'week':
                week = driver.find_element(By.ID, 'kanban')
                week_png = week.screenshot_as_png
                screen_byte = BytesIO(week_png)

            elif param_diapazon == 'today':
                todey_elem = driver.find_element(By.CLASS_NAME, 'width-15')
                todey_png = todey_elem.screenshot_as_png
                screen_byte = BytesIO(todey_png)

            elif param_diapazon == 'data' and date_event:

                element_date = driver.find_element(By.XPATH, '//input[@id="date"]')
                WebDriverWait(driver, 40).until(expected_conditions.element_to_be_clickable((By.XPATH, '//input[@id="date"]')))

                if element_date.is_displayed() and element_date.is_enabled():
                    element_date.clear()
                    element_date.send_keys(date_event) #вставляем дату в формате "23.07.2023"

                    # Кликаем на ОК в календаре, чтобы дата применилась
                    WebDriverWait(driver, 20).until(expected_conditions.element_to_be_clickable((By.XPATH,
                                                '//div[@class="drp-buttons"]/button[contains(@class, "applyBtn")]'))).click()
                    # Ждем применения даты и появления данных в таблице
                    WebDriverWait(driver, 20).until(expected_conditions.element_to_be_clickable((By.CLASS_NAME, 'width-15')))
                    # получаем данные, где первый столбец и есть наша дата
                    element = driver.find_element(By.CLASS_NAME, 'width-15')
                    events = element.find_elements(By.XPATH, './div[@class="interventions"]/a')
                    if not len(events):
                        screen_byte = f'{element.text} нет событий'
                    else:
                        png = element.screenshot_as_png
                        screen_byte = BytesIO(png)
                else:
                    # log.exception('Что-то пошло не так. Элемент даты скрыт')
                    screen_byte = f'Что-то пошло не так. Элемент даты скрыт'

            driver.quit()   # используем когда selenium/standalone-firefox
            # driver.close() # используем когда локальный браузер
        except Exception as err:
            # log.exception(err)
            # driver.close()
            driver.quit()
            return None

        return screen_byte


class TrueconfHandler:

    """
    Класс для работы с API trueconf сервера

        *-создание ссылки
        *-получение полного списка ссылок либо отфильтрованных через поиск по названию
    """
    def __init__(self):
        self.access_token = ''


    def autorization_trueconf(self):
        """
        функция выполняет "авторизацию" получает токен для работы с сервером
        """
        url = 'https://trueconf.astrobl.ru/oauth2/v1/token'

        # Получаем access_token
        response = requests.post(url, {
                'grant_type': 'client_credentials',
                'client_id': settings.CLIENT_ID,
                'client_secret': settings.CLIENT_SECRET,
            }, verify=False)
        self.access_token = response.json()['access_token']

    def create_conference(self, params_conf):
        """
        функция отправляет данные, которые предварительно были сохранены в базе при прохождении сценария,
        на сервер для создания ссылки
        """
        self.autorization_trueconf()
        url = f'https://trueconf.astrobl.ru/api/v3.3/conferences?access_token={self.access_token}'

        json_conf_data = {
            "topic": params_conf.name,  # название
            "type": params_conf.type_conf,  # режим конференции, 0- симметричная, 3 - видеоселектор
            "auto_invite": 0, # 1 - автоматическое приглашение участников виртуальной комнаты при подключении к ней
                                # любого пользователя (не только из числа приглашённых) и, как следствие, её запуске;
            "schedule": {  # режим запуска, в приведённом примере виртуальная комната (без расписания);
                "type": -1
            },
            "allow_guests": True,  # Разрешение на приглашение гостей на конференцию (не отправлял  в тесте)
            # "rights": , # флаги разрешения, скорее всего нужны
            "owner": params_conf.owner  # владелец конференции.
        }
        if params_conf.type_conf == 3 and params_conf.max_podiums:
            json_conf_data['max_podiums'] = params_conf.max_podiums # количество докладчиков в видеоселекторе]

        response = requests.post(url, json=json_conf_data, verify=False)

        if response.status_code == 200:
            return response.json()['conference']['url']
        else:
            return 'Ошибка создания ссылки'

    def get_conference_list(self, pattern=None):
        """
        функция получает список ссылок с сервера и производит фильтрацию по шаблону поиска, если он задан и возвращает
        список для отправки в чат
        """
        self.autorization_trueconf()
        url = f'https://trueconf.astrobl.ru/api/v3.3/conferences?access_token={self.access_token}'

        pattern_conf_list = []
        response = requests.get(url, verify=False)
        if response.status_code == 200:
            conferences_list = response.json()['conferences']
            if pattern:
                for conf in conferences_list:
                    if conf["topic"].lower().find(pattern.lower()) != -1:
                        pattern_conf_list.append(
                            {
                                "topic": conf["topic"],
                                "owner": conf["owner"],
                                "url": conf["url"]
                            })
            else:
                for conf in conferences_list:
                    pattern_conf_list.append(
                        {
                            "topic": conf["topic"],
                            "owner": conf["owner"],
                            "url": conf["url"]
                        })
            # print(pattern_conf_list)
            return pattern_conf_list
        else:
            return 'Не удалось получить список конференций'


# pars = ParseEvent()
# pars.request_get_organizer_location()
# pars.auth_site()
# pars.request_create_events()
# pars.parse_events('2023-7-28')
# f = pars.screen(param_diapazon='data', date_event='28.07.2023')
# print(type(f))
# trh = TrueconfHandler()
# trh.get_conference_list()

# timer_pars_loc_organiz = Timer(10, pars.request_get_organizer_location)  # неделя 604800
# timer_pars_loc_organiz.start()