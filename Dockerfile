FROM python:3.10.9

WORKDIR /usr/src/bottg

COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

COPY . /usr/src/bottg

CMD [ "python", "./botTG.py" ]