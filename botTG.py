import asyncio
from threading import Timer
from telegram import (Update,
                      InlineQueryResultArticle,
                      InputTextMessageContent,
                      InlineKeyboardButton,
                      InlineKeyboardMarkup,
                      ReplyKeyboardMarkup,
                      ReplyKeyboardRemove)
from telegram.ext import (filters,
                          MessageHandler,
                          ApplicationBuilder,
                          ContextTypes,
                          CommandHandler,
                          ConversationHandler,
                          InlineQueryHandler,
                          CallbackQueryHandler, PicklePersistence, PersistenceInput,
                          BaseHandler)
import logging
from parse_event import ParseEvent, TrueconfHandler
from io import BytesIO
from PIL import Image
import re
from pony.orm import db_session
from models import Registration
from ponypersistence import PonyPersistence
import handlers
import telegramcalendar


try:
    import settings
except ImportError:
    exit('необходимо создать совой файл настроек вместо settings.py.default и назвать settings.py')


log = logging.getLogger("Bot")

def logger():
    logging.basicConfig(
        format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
        level=logging.INFO
    )

    log.setLevel(logging.DEBUG)
    bot_formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%d-%m-%Y %H:%M:%S')
    stream_handler = logging.StreamHandler()
    stream_handler.setFormatter(bot_formatter)
    stream_handler.setLevel(logging.INFO)
    log.addHandler(stream_handler)

    file_handler = logging.FileHandler("bot.log", encoding="UTF-8")
    file_handler.setFormatter(bot_formatter)
    file_handler.setLevel(logging.DEBUG)
    log.addHandler(file_handler)
"""
    Bot telegram для работы с мероприятиями, на которых нужно участие ТП
    Функции:
        *- постинг картинки в чат
        
        *- добавление БД и запись туда инфы о пользователях
        *- сохранения прохождения сценария пользователями в БД
        *- авторизация в боте для админов и запоминание админов
        *- отправка заявки для назначения админа в чат суперпользователю с кнопками Принять или Отклонить
        *- отправка заявки создающую новое мероприятие
        *- редактирование мероприятий
"""

class BotTG:

    """
    *- создать клавиатуру внизу
    *- создать кнопки в чате
    *- обработка кнопок
    *- отправка календаря в чат и обработка кнопок
    *- отправка кнопок выбора времени
    *- кнопку На шаг назад
    *- Меню с отменой сценария
    *- удалять кнопки предыдущего сообщения
    *- множественный выбор (подсмотрел у бота @corpmsp_bot)
    *- убрать кнопку Отмена из сообщений и добавить в нижнее меню
    *- при отправке кнопки Старт удалять все сценарии пользователя и конверсейшены
    - если переход был с 5го на 8 шаг, то обратно вернуться на 5
    *- когда делал возвраты на 7 шагу и потом нажал пропустить, то ошибка изменения сообщения
    *- на первом шагу при отправке Завершить, ответ принимается
    - проверка на не пустые значения в отправке на создание мероприятия

    *- возможность создавать ссылки через бота или брать готовые через Trueconf API
    Функции суперадмина
    - у суперадмина доп функции по просмотру пользователей и возможность удаления роли админа в любой момент
    - возможность спарсить организаторов и локации в любой момент или раз в месяц
    - удаление из базы старых событий, раз в пол года

    Протестировать:
        двойной запуск, когда в таблицу с Edit запишется две записи на редактирование. при чтении из этой таблицы
        появится исключение, что может быть только одна запись.

    """
    def __init__(self, token):
        """
        Инициация бота. получение токена
        :param token:
        """

        self.WAIT_SAVE = 'WAIT_SAVE'
        self.WAIT_BUTTON = 'WAIT_BUTTON'
        self.WAIT_DATA = 'WAIT_DATA'
        self.WAIT_TIME = 'WAIT_TIME'
        self.WAIT_NAME = 'WAIT_NAME'
        self.WAIT_ORGANAIZER = 'WAIT_ORGANAIZER'
        self.WAIT_LOCATION = 'WAIT_LOCATION'
        self.WAIT_LOCATION_CUSTOM = 'WAIT_LOCATION_CUSTOM'
        self.WAIT_FORMAT = 'WAIT_FORMAT'
        self.WAIT_PLATFORM = 'WAIT_PLATFORM'
        self.WAIT_LINK = 'WAIT_LINK'
        self.WAIT_comment = 'WAIT_comment'
        self.WAIT_TYPE = 'WAIT_TYPE'
        self.WAIT_PODIUMS = 'WAIT_PODIUMS'
        self.WAIT_OWNER = 'WAIT_OWNER'
        # store_data = PersistenceInput(chat_data=False, bot_data=False, user_data=False, callback_data=False)
        # persistence = PicklePersistence(store_data=store_data, filepath="conversationbot", update_interval=3)
        persistence = PonyPersistence()
        self.application = ApplicationBuilder().token(token).persistence(persistence).build()

        self.start_handler = CommandHandler('start', self.start)
        # self.caps_handler = CommandHandler('caps', self.caps)
        # self.inline_caps_handler = InlineQueryHandler(self.inline_caps)
        self.callback_query_handler_applay = CallbackQueryHandler(self.applay_user, pattern='applay-user')
        self.callback_query_handler_reject = CallbackQueryHandler(self.reject_user, pattern='reject-user')
        self.callback_query_handler_description = CallbackQueryHandler(self.description_event, pattern='details')
        # self.callback_query_handler_done_all = CallbackQueryHandler(self.done, pattern='done')
        # self.callback_query_handler = CallbackQueryHandler(self.callback_button)

        self.message_handler_event_today = MessageHandler(filters.Regex("События сегодня") & (~filters.COMMAND) &
                                                          (~filters.Regex("^Завершить$")), self.event_today)

        self.message_handler_event_week = MessageHandler(filters.Regex("События на неделю") & (~filters.COMMAND) &
                                                          (~filters.Regex("^Завершить$")), self.event_week)

    # сценарий с календарем
        self.conv_handler_to_date = ConversationHandler(
            entry_points=[MessageHandler(filters.Regex("События на дату"), self.start_event_to_data)],
            states={
                self.WAIT_DATA: [CallbackQueryHandler(self.cal_event_data)],
                                # ,MessageHandler(filters.Regex("^\d{1,2}[,./]\d{2}[,./]\d{2}$"), self.event_data),
            },
            fallbacks=[MessageHandler(filters.Regex("^Завершить$"), self.done),
                       CallbackQueryHandler(self.done, pattern='done'),
                       CommandHandler('cancel', self.done)],
            allow_reentry=False, name="events_to_date", persistent=True)

    # сценарий с календарем
        self.conv_handler_edit_to_date = ConversationHandler(
            entry_points=[MessageHandler(filters.Regex("Подробно и Ред"), self.wait_data)],
            states={
                self.WAIT_DATA: [CallbackQueryHandler(self.cal_edit_event_to_date),
                                # MessageHandler(filters.Regex("^\d{1,2}[,./]\d{2}[,./]\d{2}$"), self.edit_event_to_date)
                                 ]},
            fallbacks=[MessageHandler(filters.Regex("^Завершить$"), self.done),
                       CallbackQueryHandler(self.done, pattern='done'),
                       CommandHandler('cancel', self.done)],
            allow_reentry=False, name="edit_events_to_date", persistent=True)


    # сценарий создания нового события
        self.conv_handler_create_event = ConversationHandler(
            entry_points=[MessageHandler(filters.Regex("Создать событие"), self.start_create_event)],
            states={
                self.WAIT_NAME: [MessageHandler(filters.TEXT & (~filters.COMMAND) & (~filters.Regex("^Завершить$")), self.name_create_event)],
                self.WAIT_ORGANAIZER: [CallbackQueryHandler(self.organizer_create_event, pattern='create_organizer'),
                                       CallbackQueryHandler(self.organizer_update_inline_button, pattern='update_create_organizer'),
                                       MessageHandler(filters.Regex("^Шаг назад$"), self.start_create_event)],
                self.WAIT_LOCATION: [CallbackQueryHandler(self.location_create_event, pattern='create_location'),
                                     CallbackQueryHandler(self.location_update_inline_button, pattern='location_update'),
                                     MessageHandler(filters.Regex("^Шаг назад$"), self.name_create_event)],
                self.WAIT_LOCATION_CUSTOM: [MessageHandler(filters.Regex("^Шаг назад$"), self.organizer_create_event),
                                            MessageHandler(filters.TEXT & (~filters.COMMAND), self.location_custom_create_event),
                                            ],
                self.WAIT_DATA: [CallbackQueryHandler(self.cal_data_create_event),
                                 MessageHandler(filters.Regex("^Шаг назад$"), self.organizer_create_event)],
                self.WAIT_TIME: [CallbackQueryHandler(self.clock_time_create_event, pattern='create_time'),
                                 MessageHandler(filters.Regex("^Шаг назад$"), self.location_create_event)],
                self.WAIT_FORMAT: [CallbackQueryHandler(self.format_create_event, pattern='create_format'),
                                   MessageHandler(filters.Regex("^Шаг назад$"), self.cal_data_create_event)],
                self.WAIT_PLATFORM: [CallbackQueryHandler(self.platform_create_event, pattern='create_platform'),
                                     MessageHandler(filters.Regex("^Шаг назад$"), self.clock_time_create_event)],
                self.WAIT_LINK: [MessageHandler(filters.Regex("^Шаг назад$") & (~filters.Regex("^Завершить$")), self.format_create_event),
                                 MessageHandler(filters.TEXT & (~filters.COMMAND), self.link_create_event),
                                 CallbackQueryHandler(self.link_create_event, pattern='create_link_skip')],
                self.WAIT_comment: [MessageHandler(filters.Regex("^Шаг назад$") & (~filters.Regex("^Завершить$")), self.platform_create_event),
                                    MessageHandler(filters.TEXT & (~filters.COMMAND), self.comment_create_event),
                                    CallbackQueryHandler(self.comment_create_event, pattern='create_comment')],
            },
            fallbacks=[MessageHandler(filters.Regex("^Завершить$"), self.done_create),
                       CommandHandler('cancel', self.done_create)],
            allow_reentry=False, name="create_event", persistent=True)

        # сценарий редактирования события
        self.conv_handler_edit_event = ConversationHandler(
            entry_points=[CallbackQueryHandler(self.start_edit_event, pattern='edit_event')],
            states={
                self.WAIT_BUTTON: [CallbackQueryHandler(self.edit_name_event, pattern='edit_name_'),
                                   CallbackQueryHandler(self.edit_organizer_event, pattern='edit_organizer_'),
                                   CallbackQueryHandler(self.edit_location_event, pattern='edit_location_'),
                                   CallbackQueryHandler(self.edit_date_event, pattern='edit_date_'),
                                   CallbackQueryHandler(self.edit_time_event, pattern='edit_time_'),
                                   CallbackQueryHandler(self.edit_format_event, pattern='edit_format_'),
                                   CallbackQueryHandler(self.edit_link_event, pattern='edit_link_'),
                                   CallbackQueryHandler(self.edit_comment_event, pattern='edit_comment_'),
                                   ],
                self.WAIT_SAVE: [CallbackQueryHandler(self.continue_edit_event, pattern='continue_edit_event'),
                                 CallbackQueryHandler(self.save_edit_event, pattern='edit_applay_')
                ],

                self.WAIT_NAME: [MessageHandler(filters.TEXT & (~filters.COMMAND) & (~filters.Regex("^Завершить$")),
                                 self.wait_new_name_event)],
                self.WAIT_ORGANAIZER: [CallbackQueryHandler(self.wait_new_organizer_event, pattern='edit_organizer'),
                                       CallbackQueryHandler(self.organizer_update_inline_button,
                                                            pattern='update_edit_organizer')],
                self.WAIT_LOCATION: [CallbackQueryHandler(self.wait_new_location_event, pattern='edit_location'),
                                     CallbackQueryHandler(self.location_update_inline_button,
                                                          pattern='update_edit_location')],
                self.WAIT_LOCATION_CUSTOM: [MessageHandler(filters.TEXT & (~filters.COMMAND),
                                                           self.wait_new_custom_location_event),],
                self.WAIT_DATA: [CallbackQueryHandler(self.cal_edit_date_event),],
                self.WAIT_TIME: [CallbackQueryHandler(self.clock_edit_time_event, pattern='edit_time'),],
                self.WAIT_FORMAT: [CallbackQueryHandler(self.wait_new_format_event, pattern='edit_format')],
                # self.WAIT_PLATFORM: [CallbackQueryHandler(self.platform_create_event, pattern='edit_platform'),
                #                      MessageHandler(filters.Regex("^Шаг назад$"), self.clock_time_create_event)],
                self.WAIT_LINK: [MessageHandler(filters.TEXT & (~filters.COMMAND) & (~filters.Regex("^Завершить$")),
                                  self.wait_new_link_event),],
                self.WAIT_comment: [MessageHandler(filters.TEXT & (~filters.COMMAND) & (~filters.Regex("^Завершить$")),
                                    self.wait_new_comment_event),],
            },
            fallbacks=[MessageHandler(filters.Regex("^Завершить$"), self.done_edit),
                       CommandHandler('cancel', self.done_edit),
                       CallbackQueryHandler(self.done_edit, pattern='cancel_edit')],
            allow_reentry=False, name="edit_event", persistent=True)

        # сценарий создания ссылки
        self.conv_handler_create_link = ConversationHandler(
            entry_points=[MessageHandler(filters.Regex("Создать ссылку"), self.start_create_link)],
            states={
                self.WAIT_NAME: [MessageHandler(filters.TEXT & (~filters.COMMAND) & (~filters.Regex("^Завершить$")),
                                 self.wait_link_name)],
                self.WAIT_TYPE: [CallbackQueryHandler(self.wait_type_conf, pattern='type_conf')],
                self.WAIT_PODIUMS: [MessageHandler(filters.TEXT & (~filters.COMMAND) & (~filters.Regex("^Завершить$")),
                                                self.wait_podiums_conf)],
                self.WAIT_OWNER: [CallbackQueryHandler(self.wait_owner_conf, pattern='owner_conf')]
            },
            fallbacks=[MessageHandler(filters.Regex("^Завершить$"), self.done_create_link),
                       CommandHandler('cancel', self.done_create_link)],
            allow_reentry=False, name="create_link", persistent=True)

        # сценарий получения списка ссылок
        self.conv_handler_get_link = ConversationHandler(
            entry_points=[MessageHandler(filters.Regex("Получить список ссылок"), self.start_get_links)],
            states={
                self.WAIT_NAME: [MessageHandler(filters.TEXT & (~filters.COMMAND) & (~filters.Regex("^Завершить$")),
                                                self.wait_link_pattern),
                                 CallbackQueryHandler(self.wait_link_pattern, pattern='get_all_link')],
                    },
            fallbacks=[MessageHandler(filters.Regex("^Завершить$"), self.done),
                       CommandHandler('cancel', self.done)],
            allow_reentry=False, name="get_links", persistent=True)


        self.handler_role_user = MessageHandler(filters.Regex("Проверить статус"), self.start)

        self.message_handler = MessageHandler(filters.TEXT & (~filters.COMMAND), self.message_text)


        self.application.add_handler(self.message_handler_event_today)
        self.application.add_handler(self.message_handler_event_week)
        self.application.add_handler(self.conv_handler_to_date)
        self.application.add_handler(self.conv_handler_edit_to_date)
        self.application.add_handler(self.conv_handler_create_event)
        self.application.add_handler(self.conv_handler_edit_event)
        self.application.add_handler(self.conv_handler_create_link)
        self.application.add_handler(self.conv_handler_get_link)
        self.application.add_handler(self.handler_role_user)
        self.application.add_handler(self.start_handler)
        self.application.add_handler(self.message_handler)

        # self.application.add_handler(self.caps_handler)
        # self.application.add_handler(self.inline_caps_handler)

        self.application.add_handler(self.callback_query_handler_applay)
        self.application.add_handler(self.callback_query_handler_reject)
        self.application.add_handler(self.callback_query_handler_description)
        # self.application.add_handler(self.callback_query_handler)

        self.parse_event = ParseEvent()
        self.trueconf_handler = TrueconfHandler()


        # парсим локации и организаторов сначала при запуске, потом раз в неделю
        self.parse_event.request_get_organizer_location()
        timer_pars_loc_organiz = Timer(604800, self.parse_event.request_get_organizer_location)  # неделя 604800 сек
        timer_pars_loc_organiz.start()



    def run(self):
        """
        Запуск бота в режиме постоянного сканирования
        """
        try:
            self.application.run_polling()
        except Exception as err:
            log.exception(err)


    async def send_error_to_superadmin(self, update: Update, context: ContextTypes.DEFAULT_TYPE, err):
        # await self.send_error_to_superadmin(update, context, 'Ошибочка')
        superadmin_id = handlers.db_get_user(role='superadmin')[0].chat_id
        log.exception(err)
        await context.bot.send_message(chat_id=superadmin_id, text=f'Ошибка в коде: {err}')

    async def applay_user(self, update: Update, context: ContextTypes.DEFAULT_TYPE):
        """
            Функция принимает заявку пользователя, подавшего запрос на присвоение роли администратора
        :param update:
        :param context:
        """
        try:
            query = update.callback_query
            await query.answer()  # ответ в ТГ, что увидели нажатую кнопку
            user_id = re.search('chat_id:\s?(\d{5,15})', update.effective_message.text)[1]
            if user_id:
                user = handlers.db_set_useradmin(user_id, 'admin')
                await context.bot.send_message(chat_id=user_id,
                           text=f"Вам предоставлен доступ к функциям бота. Проверьте статус или отправьте команду /start")

                button_list = [[InlineKeyboardButton("Отклонить", callback_data='reject-user')]]
                reply_markup = InlineKeyboardMarkup(button_list)
                await query.edit_message_text(text=update.effective_message.text,
                                              reply_markup=reply_markup)
            else:
                await context.bot.send_message(chat_id=update.effective_chat.id,
                                               text=f"Доступ не предоставлен, что-то пошло не так")
        except Exception as err:
            await self.send_error_to_superadmin(update, context, err)


    async def reject_user(self, update: Update, context: ContextTypes.DEFAULT_TYPE):
        """
            Функция отклоняет заявку пользователя, подавшего запрос на присвоение роли администратора
        :param update:
        :param context:
        """
        try:
            query = update.callback_query
            await query.answer()  # ответ в ТГ, что увидели нажатую кнопку
            user_id = re.search('chat_id:\s?(\d{5,15})', update.effective_message.text)[1]
            if user_id:
                user = handlers.db_set_useradmin(user_id, 'user')
                reply_markup = ReplyKeyboardRemove()
                await context.bot.send_message(chat_id=user_id,
                                               text=f"Ваша заявка отклонена модератором.",
                                               reply_markup=reply_markup)
                await context.bot.send_message(chat_id=update.effective_chat.id,
                                               text=f"Заявка отклонена")
            else:
                await context.bot.send_message(chat_id=update.effective_chat.id,
                                               text=f"Доступ не предоставлен, что-то пошло не так")
        except Exception as err:
            await self.send_error_to_superadmin(update, context, err)


    async def event_today(self, update: Update, context: ContextTypes.DEFAULT_TYPE):
        """
        функция отправки картинки с собтиями на сегодня
        """
        period = {'period': 'today', 'data': None}
        await self.send_photo(update, context, period)

    async def event_week(self, update: Update, context: ContextTypes.DEFAULT_TYPE):
        """
        функция отправки картинки с собтиями на неделю
        """
        period = {'period': 'week', 'data': None}
        await self.send_photo(update, context, period)

    async def start_event_to_data(self, update: Update, context: ContextTypes.DEFAULT_TYPE):
        """
            вспомогательная функция для передварительной отправки сообщения с нижним меню
        :param update:
        :param context:
        :return:
        """
        button_list = ReplyKeyboardMarkup([["Завершить"]], resize_keyboard=True)
        await context.bot.send_message(chat_id=update.effective_chat.id,
                                       text='Необходимо выбрать дату для выборки мероприятий',
                                       reply_markup=button_list)

        return await self.wait_data(update, context)

    async def wait_data(self, update: Update, context: ContextTypes.DEFAULT_TYPE, step=None) -> str:
        """
            Функция запрос даты, выводит сообщение пользователю с запрососм даты и проводит на следующий шаг по сценарию
        :param update:
        :param context:
        :return:
        """
        reply_markup=telegramcalendar.create_calendar()
        text = f"{step} Выберите дату и время" if step else f"Выберите дату"
        await context.bot.send_message(chat_id=update.effective_chat.id,
                                       text=text,
                                       reply_markup=reply_markup)
        return self.WAIT_DATA

    async def cal_event_data(self, update: Update, context: ContextTypes.DEFAULT_TYPE):
        """
            функция вызывается посде нажатия кнопки в календаре, в сценарии парсинга картинки на дату. служит для вызова
            функции cal с дополнительным параметром (передачей названия функции-обработчика даты)
        """
        return await self.cal(update, context, self.event_data)

    async def cal_edit_event_to_date(self, update: Update, context: ContextTypes.DEFAULT_TYPE):
        """
            функция вызывается посде нажатия кнопки в календаре, в сценарии парсинга событий для редактирования.
             служит для вызова функции cal с дополнительным параметром (передачей названия функции-обработчика даты)
        """
        return await self.cal(update, context, self.edit_event_to_date)

    async def cal(self, update: Update, context: ContextTypes.DEFAULT_TYPE, func_event): # ,
        """
            функция для определения выбранной даты в календаре и передачи даты в функцию-обработчик
        :param update:
        :param context:
        :param func_event: функция-обработчик
        :return:
        """
        query = update.callback_query
        if query:
            selected, date = await telegramcalendar.process_calendar_selection(update, context)

            if selected:
                return await func_event(update, context, date)
            elif date == 'done':
                handlers.db_clear_eventconversation(update.effective_chat.id)
                button_list = ReplyKeyboardMarkup(settings.BUTTONS_FOOTER, resize_keyboard=True)
                await context.bot.send_message(chat_id=update.effective_chat.id, text=f'Отменено', reply_markup=button_list)
                return ConversationHandler.END
                # return await self.done(update, context)
            else:
                await query.answer()  # ответ в ТГ, что увидели нажатую кнопку
        else:
            return await func_event(update, context)
        return None

    async def start_create_event(self, update: Update, context: ContextTypes.DEFAULT_TYPE):
        """
            функция начала сценария создания события
        """
        button_list = ReplyKeyboardMarkup([["Завершить"]], resize_keyboard=True)

        reply = await context.bot.send_message(chat_id=update.effective_chat.id,
                                               text=f"1/8 Введите название события",
                                               reply_markup=button_list)
        handlers.db_create_event(chat_id=update.effective_chat.id,
                                 message_id=reply.message_id)
        return self.WAIT_NAME

    async def name_create_event(self, update: Update, context: ContextTypes.DEFAULT_TYPE):
        """
            функция, принимает сообщение с именем события, сохраняет в БД и переводит на следующий шаг,
            с выбором организатора
        """
        button_list = ReplyKeyboardMarkup([['Шаг назад'], ["Завершить"]], resize_keyboard=True)
        await context.bot.send_message(chat_id=update.effective_chat.id,
                                       text=f"2/8 Выберите организатора",
                                       reply_markup=button_list)

        text = update.message.text

        # генерация меню с организаторами
        organizer = handlers.db_get_organizer()
        reply_markup = handlers.build_button_inline(organizer, 0, 'create_organizer')

        reply = await context.bot.send_message(chat_id=update.effective_chat.id,
                                               text=f"из списка ниже 👇",
                                               reply_markup=reply_markup)
        if text != 'Шаг назад':
            handlers.db_create_event(chat_id=update.effective_chat.id,
                                     name=text,
                                     message_id=reply.message_id)
        return self.WAIT_ORGANAIZER

    async def organizer_update_inline_button(self, update: Update, context: ContextTypes.DEFAULT_TYPE):
        """
            функция обновляет список организаторов при нажатии на кнопки лево-парво
        """
        query = update.callback_query
        update_param = query.data.split('_')
        # генерация меню с организаторами
        organizer = handlers.db_get_organizer()
        reply_markup = handlers.build_button_inline(organizer, int(update_param[-1]), f'{update_param[1]}_organizer')
        await context.bot.edit_message_text(text=query.message.text,
                                            chat_id=update.effective_chat.id,
                                            message_id=query.message.message_id,
                                            reply_markup=reply_markup
                                            )


    async def organizer_create_event(self, update: Update, context: ContextTypes.DEFAULT_TYPE):
        """
            функция обрабатывает нажатие кнопки в списке организаторов, сохраняет выбранное значение в БД и переводит
             на следующий шаг выбором места проведения
        """
        query = update.callback_query
        if query:
            organizer_num = query.data.split('_')[-1]
            await query.edit_message_text(text=query.message.text)
        else:
            organizer_num = 'шаг'
            # генерация меню с местами проведения
        location = handlers.db_get_location()
        reply_markup = handlers.build_button_inline(location, 0, 'create_location')

        reply = await context.bot.send_message(chat_id=update.effective_chat.id,
                                               text=f"3/8 Выберите место проведения",
                                               reply_markup=reply_markup)
        if organizer_num != 'шаг':
            handlers.db_create_event(chat_id=update.effective_chat.id,
                                     organizer=organizer_num,
                                     message_id=reply.message_id)

        return self.WAIT_LOCATION

    async def location_update_inline_button(self, update: Update, context: ContextTypes.DEFAULT_TYPE):
        """
            функция обновляет список мест проведения при нажатии на кнопки лево-парво
        """
        query = update.callback_query
        update_param = query.data.split('_')
        # генерация меню с местами проведения
        location = handlers.db_get_location()
        reply_markup = handlers.build_button_inline(location, int(update_param[-1]), f'{update_param[1]}_location')
        await context.bot.edit_message_text(text=query.message.text,
                                            chat_id=update.effective_chat.id,
                                            message_id=query.message.message_id,
                                            reply_markup=reply_markup
                                            )

    async def location_create_event(self, update: Update, context: ContextTypes.DEFAULT_TYPE):
        """
            функция обрабатывает нажатие кнопки в списке мест проведения, сохраняет выбранное значение в БД и переводит
            на следующий шаг либо ручной ввод места проведения, либо выбор даты проведения
        """
        query = update.callback_query
        if query:
            location_num = query.data.split('_')[-1]
            await query.edit_message_text(text=query.message.text)
            if location_num != '0': # print('место')
                handlers.db_create_event(chat_id=update.effective_chat.id, location=location_num)
                return await self.wait_data(update, context, '4/8')

            reply = await context.bot.send_message(chat_id=update.effective_chat.id,
                                           text=f"3.1/8 Введите место проведения") #, reply_markup=reply_markup
            handlers.db_create_event(chat_id=update.effective_chat.id, location=location_num, message_id=reply.message_id)
            return self.WAIT_LOCATION_CUSTOM
        else:
            handlers.db_create_event(chat_id=update.effective_chat.id, hour='', minute='')
            return await self.wait_data(update, context, '4/8')


    async def location_custom_create_event(self, update: Update, context: ContextTypes.DEFAULT_TYPE):
        """
            функция обрабатывает сообщение с местом проведения, сохраняет значение в БД и переводит
             на следующий шаг выбора даты проведения
        """
        text = update.message.text
        if text != 'Шаг назад':
            handlers.db_create_event(chat_id=update.effective_chat.id, location_custom=text)

        return await self.wait_data(update, context, '4/8')


    async def wait_time(self, update: Update, context: ContextTypes.DEFAULT_TYPE, date=None) -> str:
        """
            Функция запрос даты, выводит сообщение пользователю с запросом даты и проводит на следующий шаг по сценарию
        """
        query = update.callback_query
        reply_markup = telegramcalendar.create_clock(callback_txt='create_time')
        if query:
            handlers.db_create_event(chat_id=update.effective_chat.id, date=date)

            await context.bot.edit_message_text(text=query.message.text,
                                          chat_id=update.effective_chat.id,
                                          message_id=query.message.message_id,
                                          reply_markup=reply_markup
                                          )
        else:
            handlers.db_create_event(chat_id=update.effective_chat.id, hour='', minute='')
            await context.bot.send_message(chat_id=update.effective_chat.id,
                                           text='4/8 Выберите время',
                                           reply_markup=reply_markup
                                                )
        return self.WAIT_TIME

    async def cal_data_create_event(self, update: Update, context: ContextTypes.DEFAULT_TYPE):
        """
            функция вызывается посде нажатия кнопки в календаре, в сценарии СОЗДАНИЯ события. служит для вызова
            функции cal с дополнительным параметром (передачей названия функции-обработчика даты)
        """
        return await self.cal(update, context, self.wait_time)

    async def clock_time_create_event(self, update: Update, context: ContextTypes.DEFAULT_TYPE):
        """
            функция вызывается посде нажатия кнопки выбора вчемени, в сценарии СОЗДАНИЯ события. служит для вызова
            функции clock с дополнительным параметром (передачей названия функции-обработчика даты)
        """
        return await self.clock(update, context, self.time_create_event)

    async def clock(self, update: Update, context: ContextTypes.DEFAULT_TYPE, func_event): # ,
        """
            функция обрабатывает нажатую кнопку в выборе времени события и вызывает функцию-обработчик для выбранного
            времени
        :param func_event: функция-обработчик
        :return:
        """
        query = update.callback_query
        if query:
            callback_txt = query.data.split(';')[0]
            selected, time_ = await telegramcalendar.process_clock_selection(update, context, callback_txt=callback_txt)
        else:
            return await func_event(update, context)

        if selected:
            return await func_event(update, context, selected, time_)

        elif time_ == 'done':
            await context.bot.edit_message_text(text=query.message.text,
                                                chat_id=query.message.chat_id,
                                                message_id=query.message.message_id,
                                                )
            handlers.db_delete_event(chat_id=query.message.chat_id)
            return ConversationHandler.END
        else:
            await query.answer()  # ответ в ТГ, что увидели нажатую кнопку
        return None

    async def time_create_event(self, update: Update, context: ContextTypes.DEFAULT_TYPE, selected=None, time_=None):
        """
            функция обрабатывает и сохраняет выбранное на предыдущем шаге время и переводит на следующий шаг с выбором
            формата события
        :param selected: параметр полученный при нажати кнопки с выбором времени (часы или минуты)
        :param time_: значение полученное при нажатии кнопки выбора времени
        :return:
        """
        query = update.callback_query
        if query:
            button_list = [[InlineKeyboardButton("Онлайн", callback_data='create_format_online')],
                           [InlineKeyboardButton("Очно", callback_data='create_format_intramural')]]
            reply_markup = InlineKeyboardMarkup(button_list)
            if selected == 'h':
                handlers.db_create_event(chat_id=update.effective_chat.id, hour=time_)
                event = handlers.db_get_create_event(update.effective_chat.id)
                if event.minute:
                    await context.bot.edit_message_text(text=query.message.text,
                                                        chat_id=query.message.chat_id,
                                                        message_id=query.message.message_id,
                                                        )
                    reply = await context.bot.send_message(chat_id=update.effective_chat.id,
                                                           text=f"5/8 Выберите формат события", reply_markup=reply_markup)
                    return self.WAIT_FORMAT
            elif selected == 'm':
                handlers.db_create_event(chat_id=update.effective_chat.id, minute=time_)
                event = handlers.db_get_create_event(update.effective_chat.id)
                if event.hour:
                    # await query.answer()
                    await context.bot.edit_message_text(text=query.message.text,
                                                        chat_id=query.message.chat_id,
                                                        message_id=query.message.message_id,
                                                        )
                    reply = await context.bot.send_message(chat_id=update.effective_chat.id,
                                                           text=f"5/8 Выберите формат события", reply_markup=reply_markup)
                    return self.WAIT_FORMAT


    async def format_create_event(self, update: Update, context: ContextTypes.DEFAULT_TYPE):
        """
            функция обрабатывает выбранный формат мероприятия, сохраняет значение в БД и переводит на следующий шаг,
            в зависимости от выбора формата либо на выбор платформы для онлайн события, или на добавление комментария.
        """
        button_list = [[InlineKeyboardButton("ICEP", callback_data='create_platform_icep')],
                       [InlineKeyboardButton("ИНАЯ", callback_data='create_platform_other')]]
        reply_markup = InlineKeyboardMarkup(button_list)

        query = update.callback_query
        if query:
            format_name = query.data.split('_')[-1]
            await query.edit_message_text(text=query.message.text)
            if format_name == "intramural":
                button_list = [[InlineKeyboardButton("Пропустить", callback_data='create_comment_skip')]]
                reply_markup = InlineKeyboardMarkup(button_list)
                reply = await context.bot.send_message(chat_id=update.effective_chat.id,
                                               text=f"8/8 Добавьте комментарий", reply_markup=reply_markup)
                handlers.db_create_event(chat_id=update.effective_chat.id, format_=format_name, message_id=reply.message_id)
                return self.WAIT_comment
            reply = await context.bot.send_message(chat_id=update.effective_chat.id,
                                                   text=f"6/8 Выберите платформу", reply_markup=reply_markup)
            handlers.db_create_event(chat_id=update.effective_chat.id, format_=format_name, message_id=reply.message_id)
        else:
            reply = await context.bot.send_message(chat_id=update.effective_chat.id,
                                                    text=f"6/8 Выберите платформу", reply_markup=reply_markup)
        return self.WAIT_PLATFORM

    async def platform_create_event(self, update: Update, context: ContextTypes.DEFAULT_TYPE):
        """
            функция обрабатывает выбранную платформу. сохраняет значение в БД и переводит на следующий шаг
            запроса ссылки от пользователя
        """
        button_list = [[InlineKeyboardButton("Пропустить", callback_data='create_link_skip')]]
        reply_markup = InlineKeyboardMarkup(button_list)

        query = update.callback_query
        if query:
            platform_name = query.data.split('_')[-1]
            await query.edit_message_text(text=query.message.text)
            reply = await context.bot.send_message(chat_id=update.effective_chat.id,
                                                   text=f"7/8 Вставьте ссылку", reply_markup=reply_markup)
            handlers.db_create_event(chat_id=update.effective_chat.id, platform=platform_name,
                                     message_id=reply.message_id)
        else:
            handlers.db_create_event(chat_id=update.effective_chat.id, message_id=0)
            await context.bot.send_message(chat_id=update.effective_chat.id,
                                       text=f"7/8 Вставьте ссылку", reply_markup=reply_markup)
        return self.WAIT_LINK

    async def link_create_event(self, update: Update, context: ContextTypes.DEFAULT_TYPE):
        """
            функция обрабатывает полученое сообщение со ссылкой, записывает в БД и переводит на следующий шаг
            добавление комментария
        """
        # удаление кнопок из предыдущего сообщения
        event = handlers.db_get_create_event(chat_id=update.effective_chat.id)
        if event.message_id == 0:
            await context.bot.edit_message_text(text=f"7/8 Вставьте ссылку!", chat_id=update.effective_chat.id, message_id=event.message_id)

        query = update.callback_query
        button_list = [[InlineKeyboardButton("Пропустить", callback_data='create_comment_skip')]]
        reply_markup = InlineKeyboardMarkup(button_list)
        if query:
            link_data = query.data.split('_')[-1]

            if link_data == 'skip':
                await query.edit_message_text(text=query.message.text)
                reply = await context.bot.send_message(chat_id=update.effective_chat.id,
                                               text=f"8/8 Добавьте комментарий", reply_markup=reply_markup)
                handlers.db_create_event(chat_id=update.effective_chat.id, message_id=reply.message_id)
                return self.WAIT_comment
        else:
            text = update.message.text


            reply = await context.bot.send_message(chat_id=update.effective_chat.id,
                                           text=f"8/8 Добавьте комментарий", reply_markup=reply_markup)
            handlers.db_create_event(chat_id=update.effective_chat.id, link=text,
                                     message_id=reply.message_id)
            return self.WAIT_comment

    async def comment_create_event(self, update: Update, context: ContextTypes.DEFAULT_TYPE):
        """
            функция обрабатывает сообщение от пользователя и сохраняет комментарий в БД, затеп завершает
            сценарий создания события, отправляет полученные данные из БД на сервер для создания события и очищает
            в БД записи с данными о создании текущего события
        """
        try:
            # удаление кнопок из предыдущего сообщения
            event = handlers.db_get_create_event(chat_id=update.effective_chat.id)
            await context.bot.edit_message_text(text=f"8/8 Добавьте комментарий!", chat_id=update.effective_chat.id,
                                                message_id=event.message_id)

            query = update.callback_query
            if query:
                comment_data = query.data.split('_')[-1]
                # print(comment_data)
                if comment_data == 'skip':
                    # await query.answer()
                    await query.edit_message_text(text=query.message.text)
            else:
                text = update.message.text
                handlers.db_create_event(chat_id=update.effective_chat.id, comment=text)
            button_list = ReplyKeyboardMarkup(settings.BUTTONS_FOOTER, resize_keyboard=True)
            await context.bot.send_message(chat_id=update.effective_chat.id, text=f"Спасибо, заявка принята!", reply_markup=button_list)

            # отправка данных на создание
            await self.create_event(chat_id=update.effective_chat.id)
            # удаление данных сценария из БД
            handlers.db_delete_event(chat_id=update.effective_chat.id)
            return ConversationHandler.END
        except Exception as err:
            await self.send_error_to_superadmin(update, context, err)

    async def create_event(self, chat_id):
        """
            функция получает данные о создании события из БД и передает их в функцию для формирования запроса
            на отправку серверу, для создания события
        :param chat_id:
        """
        new_event_data = handlers.db_get_create_event(chat_id)
        self.parse_event.request_create_events(new_event_data)


    async def edit_event_to_date(self, update: Update, context: ContextTypes.DEFAULT_TYPE, calendar=''):
        """
            Функция отправляет запрос на выборку событий, на дату
            и передает информацию в функцию отправки пользователям, попутно сохраняя инфу в БД
        :param update:
        :param context:
        :return:
        """
        try:
            if calendar:
                full_data = calendar.strftime("%Y-%m-%d")
            else:
                text = update.message.text
                split_text = re.split(",|/|\.", text)
                full_data = f'20{split_text[-1]}-{split_text[1]}-{split_text[0]}'
            await context.bot.send_message(chat_id=update.effective_chat.id, text=f"Понял. Собираю данные...🧐")
            state, events = self.parse_event.parse_events(full_data)
            if state == 'OK':
                await self.send_edit_events(update, context, events)
            elif state == "NO":
                await context.bot.send_message(chat_id=update.effective_chat.id, text=events)
            elif state == 'Err':
                await context.bot.send_message(chat_id=update.effective_chat.id, text=events)

            return ConversationHandler.END
        except Exception as err:
            await self.send_error_to_superadmin(update, context, err)

    async def send_edit_events(self, update: Update, context: ContextTypes.DEFAULT_TYPE, events):
        """
            Отправка сообщений с мероприятиями и кнопками для редактирования под каждым мероприятием

        :param events: JSON с данными о мероприятии, из которых формируется сообщение на отправку
                        [{'timesec': '', 'id': '', 'data': {'name': '', 'organizer': '', 'location': '',
                        #                                 'date': '', 'format': '', 'vcs': '', 'comment': ''}}, ...... ]
        """
        for event in events:
            text = f'✅ Название: {event["data"]["name"]}\n\n' \
                   f'⏱ Время: {event["data"]["time"]}\n\n'  \
                   f'⛳️ Место: {event["data"]["location"]}\n\n' \
                   f'💻 Формат: {event["data"]["format"]}'
            # if event["data"]["format"] == 'ОНЛАЙН':
            #     text += f'\nСсылка: {event["data"]["vcs"]}'
            # if event["data"]["comment"]:
            #     text += f'\nКоммент: {event["data"]["comment"]}'

            button_list = [[InlineKeyboardButton("📜 Подробнее 📜", callback_data=f'details-{event["id"]}')],
                           [InlineKeyboardButton("✍️ Изменить ✍️", callback_data=f'edit_event_{event["id"]}')]]
            reply_markup = InlineKeyboardMarkup(button_list)
            await context.bot.send_message(chat_id=update.effective_chat.id, text=text, reply_markup=reply_markup)


    async def event_data(self, update: Update, context: ContextTypes.DEFAULT_TYPE, calendar=''):
        """
            Получаем дату от пользователя и передаем на отправку картинки
        :param update:
        :param context:
        :return:
        """
        if calendar:
            full_data = calendar.strftime("%d.%m.%Y")
        else:
            text = update.message.text
            split_text = re.split(",|/|\.", text)
            full_data = f'{split_text[0]}.{split_text[1]}.20{split_text[-1]}'
        period = {'period': 'data', 'data': full_data}
        await self.send_photo(update, context, period=period)
        return ConversationHandler.END

    async def description_event(self, update: Update, context: ContextTypes.DEFAULT_TYPE):
        """
            Отправляет в чат всплывающее окно с доп информацией о мероприятии при нажатии кнопки Подробнее
        :param update:
        :param context:
        """
        query = update.callback_query
        id_event = re.search('details-(\d{2,10})', query.data)[1]
        event = handlers.db_get_event(int(id_event))
        text = ''
        if event.format == 'ОНЛАЙН':
            text += f'📡 Ссылка: {event.vcs}'
        if event.comment:
            text += f'\n\n💬 Комментарий: {event.comment}'
        if text == '':
            text += 'Подробная информация отсутствует'
        await context.bot.answer_callback_query(update.callback_query.id, text=text, show_alert=True)
        await query.answer()  # ответ в ТГ, что увидели нажатую кнопку

    async def done_create(self, update: Update, context: ContextTypes.DEFAULT_TYPE):
        """
            функция удаляет запись с данными о создаваемом событии из БД перед завершением сценария и вызывает
            функцию принудительного завершения сценария
        """
        handlers.db_delete_event(update.effective_chat.id)
        return await self.done(update, context)

    async def done(self, update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
        """
            Функция принудительного завершения сценария
        """
        button_list = ReplyKeyboardMarkup(settings.BUTTONS_FOOTER, resize_keyboard=True)
        handlers.db_clear_eventconversation(update.effective_chat.id)
        if update.callback_query:
            query = update.callback_query
            # log.debug("событие отменено")
            await query.answer()  # ответ в ТГ, что увидели нажатую кнопку

            await query.edit_message_text(text=f"Отменено", reply_markup=button_list)
        else:
            await context.bot.send_message(chat_id=update.effective_chat.id, text=f'Отменено', reply_markup=button_list)
        return ConversationHandler.END

    async def send_photo(self, update: Update, context: ContextTypes.DEFAULT_TYPE, period):
        """
            Отправка изображения в чат бота
        :param period: словарь {'period': 'data', 'data': '31.07.2023'}
        :param update: объект, который содержит данные пользователя
        :param context: объект, который относится к принятому сообщению от пользователя
        """
        try:
            button_list = ReplyKeyboardMarkup(settings.BUTTONS_FOOTER, resize_keyboard=True)
            await context.bot.send_message(chat_id=update.effective_chat.id,
                                           text=f"Команду понял. Выполняю...🫡",
                                           reply_markup=button_list)
            img = self.parse_event.screen(period=period) #'29.07.2023'
            if type(img) == BytesIO:
                image = Image.open(img).convert('RGBA')
                temp_file = BytesIO()
                image.save(temp_file, 'png')
                temp_file.seek(0)

                await context.bot.send_photo(chat_id=update.effective_chat.id, photo=temp_file)
            elif type(img) == str:
                await context.bot.send_message(chat_id=update.effective_chat.id, text=f"{img}")
            else:
                await context.bot.send_message(chat_id=update.effective_chat.id, text="Что-то пошло не так. Картинка не получена")
        except Exception as err:
            await self.send_error_to_superadmin(update, context, err)

    async def start(self, update: Update, context: ContextTypes.DEFAULT_TYPE):
        """
            Функция, отрабатыввает при нажатии кнопки СТАРТ в боте. Определяет, относится ли пользователь к админам или нет
            регистрирует в базе и отправляет запросы суперадмину
        :param update:
        :param context:
        """
        try:
            #очистка сценария и conversation
            handlers.db_delete_event(update.effective_chat.id)
            handlers.db_clear_eventconversation(update.effective_chat.id)

            user = handlers.db_get_user(update.effective_chat.id)
            if user is not None and (user.role == 'admin' or user.role == 'superadmin'):
                # отправка нижнего меню с приветствием
                markup_footer = ReplyKeyboardMarkup(settings.BUTTONS_FOOTER, resize_keyboard=True)
                await update.message.reply_text("Добро пожаловать в бот. Вам доступны функции администратора",
                                                reply_markup=markup_footer)
            elif user is not None and user.role == 'user':
                # удаление нижнего меню  reply_markup=ReplyKeyboardRemove()
                markup_user = ReplyKeyboardMarkup(settings.BUTTONS_USER, resize_keyboard=True) #, one_time_keyboard=True
                await update.message.reply_text(f"Вам еще не доступны функции бота, Ваша заявка на рассмотрении у модеоатора",
                                                reply_markup=markup_user)
                # Отправка заявки в чат супера
                await self.send_to_superuser(update, context)
            else:
                handlers.db_registration(update.effective_chat, 'user')
                markup_user = ReplyKeyboardMarkup(settings.BUTTONS_USER, resize_keyboard=True)
                await update.message.reply_text("Добро пожаловать в бота. Данные отправлены модератору на рассмотрение",
                                                reply_markup=markup_user)
                # Отправка заявки в чат супера
                await self.send_to_superuser(update, context)
        except Exception as err:
            await self.send_error_to_superadmin(update, context, err)


    async def send_to_superuser(self, update: Update, context: ContextTypes.DEFAULT_TYPE):
        """
            Функция отправки заявки от пользователя на добавление его в администраторы бота
        :param update:
        :param context:
        """
        try:
            chat_id_super = handlers.db_get_user(role='superadmin')[0].chat_id
            user_inf = update.effective_chat

            button_list = [[InlineKeyboardButton("Принять", callback_data='applay-user')],
                           [InlineKeyboardButton("Отклонить", callback_data='reject-user')]]
            reply_markup = InlineKeyboardMarkup(button_list)
            await context.bot.send_message(chat_id=chat_id_super,
                                           text=f"Новый пользователь: \nИмя: {user_inf.effective_name} "
                                                f"\nЛогин: {user_inf.username} \nchat_id: {user_inf.id}",
                                           reply_markup=reply_markup)
        except Exception as err:
            await self.send_error_to_superadmin(update, context, err)

    async def start_edit_event(self, update: Update, context: ContextTypes.DEFAULT_TYPE):
        """
            функция начала сценария редактирования события. создает запись с номером события в БД,
            затем заполняет все столбцы текущими значениями имеющигося мероприятия. необходимо для отпраки этих
            данных на сервер. в случае редактирования только одного параметра события, отправить нужно все параметры.
            и выводит сообщение с кнопками для выбора, что нужно отредактировать
        """
        params_event = {}
        try:
            query = update.callback_query
            if query:
                params_event['chat_id'] = update.effective_chat.id
                num_event = query.data.split('_')[-1]
                params_event['event_id'] = num_event
                await query.answer()

                button_list = ReplyKeyboardMarkup([["Завершить"]], resize_keyboard=True)
                await context.bot.send_message(chat_id=update.effective_chat.id,
                                               text=f"Сценарий изменения события",
                                               reply_markup=button_list)
                # создаем в базе запись
                handlers.db_edit_event(chat_id=update.effective_chat.id, event_id=num_event)
                event = handlers.db_get_event(num_event)
                params_event['name'] = event.name

                params_event['organizer'] = handlers.db_get_organizer_position_id(event.organizer)[0].position_id
                location = handlers.db_get_location_position_id(event.location)[0]
                if not location:
                    params_event['location'] = 0
                    params_event['location_custom'] = event.location
                else:
                    params_event['location'] = location.position_id
                    params_event['location_custom'] = None

                params_event['date'], params_event['hour'], params_event['minute'] = handlers.data_parse(event.time)

                params_event['format_'] = settings.fotmats_to_param[event.format]

                if event.vcs:
                    params_event['platform'] = 'icep' if event.vcs.find('trueconf.astrobl') != -1 else 'other'
                else:
                    params_event['platform'] = None

                params_event['link'] = event.vcs
                params_event['comment'] = event.comment if event.comment else None

                """
                # params_event = {"chat_id": update.effective_chat.id,
                                # "event_id": num_event,
                                # "name": event.name,
                                # "organizer": organizer.position_id,
                                # "location": location_id,
                                # "location_custom": location_cust,
                                # "date": date,
                                # "hour": hour,
                                # "minute": minute,
                                # "format_": format_event,
                                # "platform": platform_ivent,
                                # "link": event.vcs,
                                # "comment": event.comment #
                # }
                """

                handlers.db_edit_event(**params_event)

                button_list = handlers.edit_button_menu(num_event)
                reply_markup = InlineKeyboardMarkup(button_list)

                await context.bot.send_message(chat_id=update.effective_chat.id,
                                               text=f"Что нужно изменить?",
                                               reply_markup=reply_markup)

            return self.WAIT_BUTTON
        except Exception as err:
            await self.send_error_to_superadmin(update, context, err)

    async def edit_name_event(self, update: Update, context: ContextTypes.DEFAULT_TYPE):
        """
            функция выводит текущее название события и переходит на шаг ожидания ввода нового названия
        """
        query = update.callback_query
        if query:
            await query.edit_message_text(text=query.message.text)
            num_event = query.data.split('_')[-1]
            event_data = handlers.db_get_event(num_event)
            await context.bot.send_message(chat_id=update.effective_chat.id,
                                           text=f"🔹Текущее название: {event_data.name}",
                                           ) # reply_markup=reply_markup
            await context.bot.send_message(chat_id=update.effective_chat.id,
                                           text=f"Введите новое название события👇",
                                           )    #reply_markup=button_list
        return self.WAIT_NAME

    async def wait_new_name_event(self, update: Update, context: ContextTypes.DEFAULT_TYPE):
        """
            функция обрабатывает полученное сообщение с новым названием события и выдает сообщение с запросом
            применить изменения или продолжить редактирование
        """
        text = update.message.text
        handlers.db_edit_event(chat_id=update.effective_chat.id, name=text, status='edit')
        return await self.wait_save(update, context)

    async def edit_organizer_event(self, update: Update, context: ContextTypes.DEFAULT_TYPE):
        """
            функция выводит текущего организатора события и переходит на шаг ожидания выбора нового организатора
        """
        query = update.callback_query
        if query:
            await query.edit_message_text(text=query.message.text)
            num_event = query.data.split('_')[-1]
            event_data = handlers.db_get_event(num_event)

            # генерация меню с организаторами
            organizer = handlers.db_get_organizer()
            reply_markup = handlers.build_button_inline(organizer, 0, 'edit_organizer')
            await context.bot.send_message(chat_id=update.effective_chat.id,
                                           text=f"🔹Текущий организатор: {event_data.organizer}",
                                           ) # reply_markup=reply_markup
            await context.bot.send_message(chat_id=update.effective_chat.id,
                                           text=f"Выберите организатора👇",
                                           reply_markup=reply_markup)
        return self.WAIT_ORGANAIZER

    async def wait_new_organizer_event(self, update: Update, context: ContextTypes.DEFAULT_TYPE):
        """
            функция обрабатывает полученное сообщение с выбранным организатором события и выдает сообщение с запросом
            применить изменения или продолжить редактирование
        """
        query = update.callback_query
        if query:
            await query.edit_message_text(text=query.message.text)
            num_organizer = query.data.split('_')[-1]

            handlers.db_edit_event(chat_id=update.effective_chat.id, organizer=num_organizer, status='edit')

            return await self.wait_save(update, context)

    async def edit_location_event(self, update: Update, context: ContextTypes.DEFAULT_TYPE):
        """
            функция выводит текущее место проведения события и переходит на шаг ожидания выбора нового места проведения
        """
        query = update.callback_query
        if query:
            await query.edit_message_text(text=query.message.text)
            num_event = query.data.split('_')[-1]
            event_data = handlers.db_get_event(num_event)

            # генерация меню с локациями
            location = handlers.db_get_location()
            reply_markup = handlers.build_button_inline(location, 0, 'edit_location')
            await context.bot.send_message(chat_id=update.effective_chat.id,
                                           text=f"🔹Текущее место проведения: {event_data.location}",
                                           )
            await context.bot.send_message(chat_id=update.effective_chat.id,
                                           text=f"Выберите место проведения👇",
                                           reply_markup=reply_markup)
        return self.WAIT_LOCATION

    async def wait_new_location_event(self, update: Update, context: ContextTypes.DEFAULT_TYPE):
        """
            функция обрабатывает полученное сообщение с выбранным места проведения события или переходит на шаг
            ввода другого места проведения, которого нет в списке и выдает сообщение с запросом
            применить изменения или продолжить редактирование
        """
        query = update.callback_query
        if query:
            await query.edit_message_text(text=query.message.text)
            num_location = query.data.split('_')[-1]

            handlers.db_edit_event(chat_id=update.effective_chat.id, location=num_location, status='edit')

            if num_location == '0':
                await context.bot.send_message(chat_id=update.effective_chat.id,
                                           text=f"Введите место проведения👇")
                return self.WAIT_LOCATION_CUSTOM

            return await self.wait_save(update, context)

    async def wait_new_custom_location_event(self, update: Update, context: ContextTypes.DEFAULT_TYPE):
        """
            функция обрабатывает полученное сообщение с местом проведения события и выдает сообщение с запросом
            применить изменения или продолжить редактирование
        """
        text = update.message.text
        handlers.db_edit_event(chat_id=update.effective_chat.id, location_custom=text, status='edit')
        return await self.wait_save(update, context)

    async def edit_date_event(self, update: Update, context: ContextTypes.DEFAULT_TYPE):
        """
            функция выводит текущую дату проведения события и переходит на шаг ожидания выбора новой даты проведения
        """
        query = update.callback_query
        if query:
            await query.edit_message_text(text=query.message.text)
            num_event = query.data.split('_')[-1]
            event_data = handlers.db_get_event(num_event)

            await context.bot.send_message(chat_id=update.effective_chat.id,
                                           text=f"🔹Текущая дата: {event_data.time[:-8]}",
                                           )
        return await self.wait_data(update, context)

    async def cal_edit_date_event(self, update: Update, context: ContextTypes.DEFAULT_TYPE):
        """
            вспомогательная функция которая вызывается посде нажатия кнопки в календаре, в сценарии редактирования
            события. служит для вызова функции cal с дополнительным параметром
            (передачей названия функции-обработчика даты)
        """
        return await self.cal(update, context, self.wait_edit_date_event)

    async def wait_edit_date_event(self, update: Update, context: ContextTypes.DEFAULT_TYPE, date) -> str:
        """
            функция принимает значение даты, полученное при нажатии пользователем кнопки календаря и сохраняет
            значение в БД  и выдает сообщение с запросом применить изменения или продолжить редактирование
        :param date: дата
        """
        query = update.callback_query
        if query:
            handlers.db_edit_event(chat_id=update.effective_chat.id, date=date.strftime("%d.%m.%Y"), status='edit')

            return await self.wait_save(update, context)


    async def edit_time_event(self, update: Update, context: ContextTypes.DEFAULT_TYPE):
        """
            функция выводит текущее время проведения события и переходит на шаг ожидания выбора нового времени проведения
        """
        query = update.callback_query
        if query:
            await query.edit_message_text(text=query.message.text)
            num_event = query.data.split('_')[-1]
            event_data = handlers.db_get_event(num_event)

            await context.bot.send_message(chat_id=update.effective_chat.id,
                                           text=f"🔹Текущее время: {event_data.time[-5:]}",
                                           )
            handlers.db_edit_event(chat_id=update.effective_chat.id, hour='', minute='', status='edit')
            reply_markup = telegramcalendar.create_clock(callback_txt='edit_time')
            await context.bot.send_message(chat_id=update.effective_chat.id,
                                           text='Выберите время',
                                           reply_markup=reply_markup
                                           )
            return self.WAIT_TIME

    async def clock_edit_time_event(self, update: Update, context: ContextTypes.DEFAULT_TYPE):
        """
            вспомогательная функция которая вызывается посде нажатия кнопки выбора времени, в сценарии редактирования
            события. служит для вызова функции clock с дополнительным параметром
            (передачей названия функции-обработчика даты)
        """
        return await self.clock(update, context, self.wait_clock_edit_time_event)

    async def wait_clock_edit_time_event(self, update: Update, context: ContextTypes.DEFAULT_TYPE, selected=None, time_=None):
        """
            функция обрабатывает и сохраняет выбранное время и выдает сообщение с запросом применить
             изменения или продолжить редактирование
        :param selected: параметр полученный при нажати кнопки с выбором времени (часы или минуты)
        :param time_: значение полученное при нажатии кнопки выбора времени
        :return:
        """
        query = update.callback_query
        if query:
            if selected == 'h':
                handlers.db_edit_event(chat_id=update.effective_chat.id, hour=time_, status='edit')

                event = handlers.db_get_edit_event(update.effective_chat.id)
                if event.minute:
                    await context.bot.edit_message_text(text=query.message.text,
                                                        chat_id=query.message.chat_id,
                                                        message_id=query.message.message_id,
                                                        )
                    return await self.wait_save(update, context)
            elif selected == 'm':
                handlers.db_edit_event(chat_id=update.effective_chat.id, minute=time_, status='edit')
                event = handlers.db_get_edit_event(update.effective_chat.id)

                if event.hour:
                    await context.bot.edit_message_text(text=query.message.text,
                                                        chat_id=query.message.chat_id,
                                                        message_id=query.message.message_id,
                                                        )
                    return await self.wait_save(update, context)

    async def edit_format_event(self, update: Update, context: ContextTypes.DEFAULT_TYPE):
        """
            функция выводит текущий формат проведения события и переходит на шаг ожидания выбора
            нового формата проведения
        """
        query = update.callback_query
        if query:
            await query.edit_message_text(text=query.message.text)
            num_event = query.data.split('_')[-1]
            event_data = handlers.db_get_event(num_event)

            button_list = [[InlineKeyboardButton("Онлайн", callback_data='edit_format_online')],
                           [InlineKeyboardButton("Очно", callback_data='edit_format_intramural')]]
            reply_markup = InlineKeyboardMarkup(button_list)
            await context.bot.send_message(chat_id=update.effective_chat.id,
                                           text=f"🔹Текущий формат проведения: {event_data.format}",
                                           )
            await context.bot.send_message(chat_id=update.effective_chat.id,
                                           text=f"Выберите формат👇",
                                           reply_markup=reply_markup)
        return self.WAIT_FORMAT

    async def wait_new_format_event(self, update: Update, context: ContextTypes.DEFAULT_TYPE):
        """
            функция обрабатывает и сохраняет выбранный формат и выдает сообщение с запросом применить
            изменения или продолжить редактирование
        """
        query = update.callback_query
        if query:
            await query.edit_message_text(text=query.message.text)
            new_format = query.data.split('_')[-1]

            handlers.db_edit_event(chat_id=update.effective_chat.id, format_=new_format, status='edit')

            return await self.wait_save(update, context)

    async def edit_link_event(self, update: Update, context: ContextTypes.DEFAULT_TYPE):
        """
            функция выводит текущую ссылку и переходит на шаг ожидания ввода
            новой ссылки
        """
        query = update.callback_query
        if query:
            await query.edit_message_text(text=query.message.text)
            num_event = query.data.split('_')[-1]
            event_data = handlers.db_get_event(num_event)

            await context.bot.send_message(chat_id=update.effective_chat.id,
                                           text=f"🔹Текущая ссылка: {event_data.vcs}",
                                           )
            await context.bot.send_message(chat_id=update.effective_chat.id,
                                           text=f"Введите ссылку👇",
                                           )
        return self.WAIT_LINK

    async def wait_new_link_event(self, update: Update, context: ContextTypes.DEFAULT_TYPE):
        """
            функция обрабатывает и сохраняет введенную ссылку и выдает сообщение с запросом применить
            изменения или продолжить редактирование
        """
        text = update.message.text
        handlers.db_edit_event(chat_id=update.effective_chat.id, link=text, status='edit')
        return await self.wait_save(update, context)

    async def edit_comment_event(self, update: Update, context: ContextTypes.DEFAULT_TYPE):
        """
            функция выводит текущий комментарий и переходит на шаг ожидания ввода
            нового комментария
        """
        query = update.callback_query
        if query:
            await query.edit_message_text(text=query.message.text)
            num_event = query.data.split('_')[-1]
            event_data = handlers.db_get_event(num_event)

            await context.bot.send_message(chat_id=update.effective_chat.id,
                                           text=f"🔹Текущий комментарий: {event_data.comment}",
                                           )
            await context.bot.send_message(chat_id=update.effective_chat.id,
                                           text=f"Введите комментарий👇",
                                           )
        return self.WAIT_comment

    async def wait_new_comment_event(self, update: Update, context: ContextTypes.DEFAULT_TYPE):
        """
            функция обрабатывает и сохраняет введенный комментарий и выдает сообщение с запросом применить
            изменения или продолжить редактирование
        """
        text = update.message.text
        handlers.db_edit_event(chat_id=update.effective_chat.id, comment=text, status='edit')
        return await self.wait_save(update, context)

    async def done_edit(self, update: Update, context: ContextTypes.DEFAULT_TYPE):
        """
            вспомогательная функция для удаления записи с данными о редактировании мероприятия из БД, которая вызывает
            функцию принудительного завершения сценария редактирования
        """
        handlers.db_delete_edit_event(update.effective_chat.id)
        return await self.done(update, context)

    async def wait_save(self, update: Update, context: ContextTypes.DEFAULT_TYPE):
        """
            функция вывода ссобщения с кнопками применить изменения или продолжить редактирование
        """
        event_edit = handlers.db_get_edit_event(update.effective_chat.id)
        if event_edit:
            button_list = [[InlineKeyboardButton("Применить изменения", callback_data=f'edit_applay_{event_edit.event_id}')],
                           [InlineKeyboardButton("Продолжить редактирование", callback_data=f'continue_edit_event_{event_edit.event_id}')]]
            reply_markup = InlineKeyboardMarkup(button_list)
            await context.bot.send_message(chat_id=update.effective_chat.id,
                                           text=f"Принял. Что дальше?",
                                           reply_markup=reply_markup)
            return self.WAIT_SAVE

    async def save_edit_event(self, update: Update, context: ContextTypes.DEFAULT_TYPE):
        """
            функция сохранения отредактированных данных (передача данных на сервер для применения изменений)
        """
        try:
            query = update.callback_query
            if query:
                await query.edit_message_text(text=query.message.text)
            event_edit_data = handlers.db_get_edit_event(update.effective_chat.id)
            if event_edit_data.status == 'edit':
                if event_edit_data.format == 'online' and not event_edit_data.platform:
                    if event_edit_data.link:
                        platform_ivent = 'icep' if event_edit_data.link.find('trueconf.astrobl') != -1 else 'other'
                        handlers.db_edit_event(chat_id=update.effective_chat.id, platform=platform_ivent)
                        event_edit_data = handlers.db_get_edit_event(update.effective_chat.id)
                    else:
                        await context.bot.send_message(chat_id=update.effective_chat.id,
                                                       text=f"Для продолжения необходимо ввести ссылку👇",
                                                       )
                        return self.WAIT_LINK
                    # вызов функции формирования данных и отправки этих данных на сервер
                self.parse_event.request_edit_events(data=event_edit_data)
                # удаление записи о редактировании мероприятия из БД
                handlers.db_delete_edit_event(update.effective_chat.id)
                # формирование основного нижнего меню бота и подтверждение об изменении события
                button_list = ReplyKeyboardMarkup(settings.BUTTONS_FOOTER, resize_keyboard=True)
                await context.bot.send_message(chat_id=update.effective_chat.id,
                                               text=f"Событие изменено❗️",
                                               reply_markup=button_list)
            return ConversationHandler.END
        except Exception as err:
            await self.send_error_to_superadmin(update, context, err)

    async def continue_edit_event(self, update: Update, context: ContextTypes.DEFAULT_TYPE):
        """
            функция продолжения редактирования. выдает сообщение с вариантами выбора, что необходимо изменить
        """
        query = update.callback_query
        if query:
            await query.edit_message_text(text=query.message.text)
            num_event = query.data.split('_')[-1]

            button_list = handlers.edit_button_menu(num_event)
            reply_markup = InlineKeyboardMarkup(button_list)

            await context.bot.send_message(chat_id=update.effective_chat.id,
                                           text=f"Что нужно изменить?",
                                           reply_markup=reply_markup)

            return self.WAIT_BUTTON


    async def start_create_link(self, update: Update, context: ContextTypes.DEFAULT_TYPE):
        """
           функция начала сценария создания ссылки
       """
        button_list = ReplyKeyboardMarkup([["Завершить"]], resize_keyboard=True)

        reply = await context.bot.send_message(chat_id=update.effective_chat.id,
                                               text=f"Введите название конференции",
                                               reply_markup=button_list)
        handlers.db_create_link(chat_id=update.effective_chat.id)
        return self.WAIT_NAME

    async def wait_link_name(self, update: Update, context: ContextTypes.DEFAULT_TYPE):
        """
            функция обрабатывает полученное сообщение с названием конференции и переходит
            на следующий шаг запроса типа конференции
        """
        try:
            text = update.message.text
            handlers.db_create_link(chat_id=update.effective_chat.id, name=text)

            button_list = [[InlineKeyboardButton("Симметричная", callback_data='type_conf_0')],
                           [InlineKeyboardButton("Видеоселектор", callback_data='type_conf_3')]]
            reply_markup = InlineKeyboardMarkup(button_list)
            await context.bot.send_message(chat_id=update.effective_chat.id,
                                           text=f"Выберите тип конференции👇",
                                           reply_markup=reply_markup)
            return self.WAIT_TYPE
        except Exception as err:
            await self.send_error_to_superadmin(update, context, err)

    async def wait_type_conf(self, update: Update, context: ContextTypes.DEFAULT_TYPE):
        """
            функция обрабатывает и сохраняет выбранный тип конференции и переходит на следующий шаг в
            зависимости от типа конфы
        """
        query = update.callback_query
        if query:
            await query.edit_message_text(text=query.message.text)
            type_conf = query.data.split('_')[-1]

            handlers.db_create_link(chat_id=update.effective_chat.id, type_conf=int(type_conf))

            if type_conf == '0':
                button_list = [[InlineKeyboardButton("Ситуационный центр", callback_data='owner_conf_smallhall')],
                               [InlineKeyboardButton("Большой зал>", callback_data='owner_conf_bighall')],
                               [InlineKeyboardButton("apopovich", callback_data='owner_conf_apopovich')],
                               [InlineKeyboardButton("radim", callback_data='owner_conf_radim')],
                               [InlineKeyboardButton("Богомолов", callback_data='owner_conf_mbogomolov')],
                               [InlineKeyboardButton("Афанасьев", callback_data='owner_conf_dafanasyev')],
                               [InlineKeyboardButton("Юнусов Румиль", callback_data='owner_conf_riyunusov')],
                               [InlineKeyboardButton("Набутовский", callback_data='owner_conf_anabutovsky')],
                               [InlineKeyboardButton("Ядыкин", callback_data='owner_conf_ryadykin')],
                               [InlineKeyboardButton("Гаджиев", callback_data='owner_conf_msgadzhiev')],
                               [InlineKeyboardButton("Волынский", callback_data='owner_conf_ivolynskiy')],
                               [InlineKeyboardButton("Кучина", callback_data='owner_conf_okuchina')],
                               ]
                reply_markup = InlineKeyboardMarkup(button_list)
                await context.bot.send_message(chat_id=update.effective_chat.id,
                                               text=f"Выберите владельца 👇",
                                               reply_markup=reply_markup)
                return self.WAIT_OWNER
            elif type_conf == '3':
                await context.bot.send_message(chat_id=update.effective_chat.id,
                                               text=f"Введите количество докладчиков от 1 до 36 👇",
                                               )
                return self.WAIT_PODIUMS

    async def wait_podiums_conf(self, update: Update, context: ContextTypes.DEFAULT_TYPE):
        """
            функция обрабатывает и сохраняет введенное количество докладчиков и переходит на следующий шаг
            выбора владельца
        """

        text = update.message.text
        if text.isdigit() and 0 < int(text) < 37:

            handlers.db_create_link(chat_id=update.effective_chat.id, max_podiums=int(text))

            button_list = [[InlineKeyboardButton("Ситуационный центр", callback_data='owner_conf_smallhall')],
                           [InlineKeyboardButton("Большой зал>", callback_data='owner_conf_bighall')],
                           [InlineKeyboardButton("apopovich", callback_data='owner_conf_apopovich')],
                           [InlineKeyboardButton("radim", callback_data='owner_conf_radim')],
                           [InlineKeyboardButton("Богомолов", callback_data='owner_conf_mbogomolov')],
                           [InlineKeyboardButton("Афанасьев", callback_data='owner_conf_dafanasyev')],
                           [InlineKeyboardButton("Юнусов Румиль", callback_data='owner_conf_riyunusov')],
                           [InlineKeyboardButton("Набутовский", callback_data='owner_conf_anabutovsky')],
                           [InlineKeyboardButton("Ядыкин", callback_data='owner_conf_ryadykin')],
                           [InlineKeyboardButton("Гаджиев", callback_data='owner_conf_msgadzhiev')],
                           [InlineKeyboardButton("Волынский", callback_data='owner_conf_ivolynskiy')],
                           [InlineKeyboardButton("Кучина", callback_data='owner_conf_okuchina')],
                           ]
            reply_markup = InlineKeyboardMarkup(button_list)
            await context.bot.send_message(chat_id=update.effective_chat.id,
                                           text=f"Выберите владельца 👇",
                                           reply_markup=reply_markup)
            return self.WAIT_OWNER

        else:
            await context.bot.send_message(chat_id=update.effective_chat.id,
                                           text=f"Необходимо ввести только цифру от 1 до 36 👇")


    async def wait_owner_conf(self, update: Update, context: ContextTypes.DEFAULT_TYPE):
        """
            функция обрабатывает и сохраняет выбранного владельца и переходит на следующий шаг отправки данных на сервер
        """
        try:
            query = update.callback_query
            if query:
                await query.edit_message_text(text=query.message.text)
                owner_conf = query.data.split('_')[-1]

                handlers.db_create_link(chat_id=update.effective_chat.id, owner=owner_conf)

                # получение инфы для создания из БД
                data_conf = handlers.db_get_create_link(chat_id=update.effective_chat.id)
                # отправка на сервер и ожидание ответа
                link = self.trueconf_handler.create_conference(data_conf)
                # удаление данных из БД
                handlers.db_delete_create_link(chat_id=update.effective_chat.id)
                button_list = ReplyKeyboardMarkup(settings.BUTTONS_FOOTER, resize_keyboard=True)
                await context.bot.send_message(chat_id=update.effective_chat.id,
                                               text=f"Результат:",
                                               )
                await context.bot.send_message(chat_id=update.effective_chat.id,
                                               text=f"{link}",
                                               reply_markup=button_list)

            return ConversationHandler.END
        except Exception as err:
            await self.send_error_to_superadmin(update, context, err)

    async def done_create_link(self, update: Update, context: ContextTypes.DEFAULT_TYPE ):
        """
            вспомогательная функция для удаления записи с данными о создании ссылки из БД, которая вызывает
            функцию принудительного завершения сценария создания
        """
        handlers.db_delete_create_link(update.effective_chat.id)
        return await self.done(update, context)

    async def start_get_links(self, update: Update, context: ContextTypes.DEFAULT_TYPE):
        """
           функция начала сценария получения списка ссылок
       """
        button_list = ReplyKeyboardMarkup([["Завершить"]], resize_keyboard=True)
        await context.bot.send_message(chat_id=update.effective_chat.id,
                                               text=f"Если хотите получить не весь список ссылок, отправьте сообщение "
                                                    f"с текстом для выборки",
                                               reply_markup=button_list)

        button_ = [[InlineKeyboardButton("Весь список", callback_data='get_all_link')]]
        reply_markup = InlineKeyboardMarkup(button_)
        await context.bot.send_message(chat_id=update.effective_chat.id,
                                               text=f"Введите шаблон поиска",
                                               reply_markup=reply_markup)
        return self.WAIT_NAME

    async def wait_link_pattern(self, update: Update, context: ContextTypes.DEFAULT_TYPE):
        """
            функция обрабатывает полученное сообщение с частью названия конференции и переходит
            на следующий шаг запроса типа конференции
        """
        try:
            link_list = f''
            query = update.callback_query
            if query:
                await query.edit_message_text(text=query.message.text)
                links_dict = self.trueconf_handler.get_conference_list()
            else:
                text = update.message.text
                links_dict = self.trueconf_handler.get_conference_list(text)

            button_list = ReplyKeyboardMarkup(settings.BUTTONS_FOOTER, resize_keyboard=True)
            for link in links_dict:
                link_list += f'{link["topic"]}\n{link["owner"]}\n{link["url"]}\n\n'
                if len(link_list) > 3000:
                    await context.bot.send_message(chat_id=update.effective_chat.id,
                                                   text=link_list,
                                                   reply_markup=button_list, disable_web_page_preview=True)
                    link_list = ''
                else:
                    continue
            if link_list:
                await context.bot.send_message(chat_id=update.effective_chat.id,
                                               text=link_list,
                                               reply_markup=button_list, disable_web_page_preview=True)
            return ConversationHandler.END
        except Exception as err:
            await self.send_error_to_superadmin(update, context, err)


    async def message_text(self, update: Update, context: ContextTypes.DEFAULT_TYPE):
        """
            Обработка текстовых сообщений от пользователя
        :param update:
        :param context:
        """
        text = update.message.text
        for button in settings.BUTTONS_FOOTER:
            if text in button:
                await context.bot.send_message(chat_id=update.effective_chat.id,
                                               text=f'Вы находитесь в сценарии. Завершите все сценарии через меню в строке ввода',
                                               )
                return None
        else:
            await context.bot.send_message(chat_id=update.effective_chat.id, text='не распознал')


    async def callback_button(self, update: Update, context: ContextTypes.DEFAULT_TYPE):
        query = update.callback_query
        variant = query.data
        await query.answer()    # ответ в ТГ, что увидели нажатую кнопку
        await context.bot.send_message(chat_id=update.effective_chat.id, text=f'{variant}')  # 'Нажата кнопка'



    # async def caps(self, update: Update, context: ContextTypes.DEFAULT_TYPE):
    #     text_caps = ' '.join(context.args).upper()
    #     await context.bot.send_message(chat_id=update.effective_chat.id, text=text_caps)
    #
    # async def inline_caps(self, update: Update, context: ContextTypes.DEFAULT_TYPE):
    #     query = update.inline_query.query
    #     if not query:
    #         return
    #     results = []
    #     results.append(
    #         InlineQueryResultArticle(
    #             id=query.upper(),
    #             title='CAPS',
    #             input_message_content=InputTextMessageContent(query.upper())
    #             ))
    #     await context.bot.answer_inline_query(update.inline_query.id, results)
#

if __name__ == '__main__':
    logger()
    bottg = BotTG(settings.TOKEN)
    bottg.run()









