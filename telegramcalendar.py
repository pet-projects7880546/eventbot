#!/usr/bin/env python3
#
# A library that allows to create an inline calendar keyboard.
# grcanosa https://github.com/grcanosa
#
"""
Base methods for calendar keyboard creation and processing.
"""

from telegram import InlineKeyboardButton, InlineKeyboardMarkup, ReplyKeyboardRemove
from datetime import datetime, timedelta
import calendar
import asyncio



def create_callback_data(action, year, month, day):
    """ Create the callback data associated to each button"""
    return ";".join([action, str(year), str(month), str(day)])


def separate_callback_data(data):
    """ Separate the callback data"""
    return data.split(";")


def create_calendar(year=None, month=None):
    """
    Create an inline keyboard with the provided year and month
    :param int year: Year to use in the calendar, if None the current year is used.
    :param int month: Month to use in the calendar, if None the current month is used.
    :return: Returns the InlineKeyboardMarkup object with the calendar.
    """
    now = datetime.now()
    if year == None: year = now.year
    if month == None: month = now.month
    data_ignore = create_callback_data("IGNORE", year, month, 0)
    data_done = create_callback_data("done", year, month, 0)
    data_today = create_callback_data("TODAY", year, month, now.day)
    keyboard = []
    # First row - Month and Year
    row = []
    row.append(InlineKeyboardButton(calendar.month_name[month] + " " + str(year), callback_data=data_ignore))
    keyboard.append(row)
    # Second row - Week Days
    row = []
    for day in ["Пн", "Вт", "Ср", "Чт", "Пт", "Сб", "Вс"]:
        row.append(InlineKeyboardButton(day, callback_data=data_ignore))
    keyboard.append(row)

    my_calendar = calendar.monthcalendar(year, month)
    for week in my_calendar:
        row = []
        for day in week:
            if (day == 0):
                row.append(InlineKeyboardButton(" ", callback_data=data_ignore))
            else:
                row.append(InlineKeyboardButton(str(day), callback_data=create_callback_data("DAY", year, month, day)))
        keyboard.append(row)
    # Last row - Buttons
    row = []
    row.append(InlineKeyboardButton("<", callback_data=create_callback_data("PREV-MONTH", year, month, day)))
    # row.append(InlineKeyboardButton("Отмена", callback_data=data_done))
    row.append(InlineKeyboardButton("Сегодня", callback_data=data_today))
    row.append(InlineKeyboardButton(">", callback_data=create_callback_data("NEXT-MONTH", year, month, day)))
    keyboard.append(row)

    return InlineKeyboardMarkup(keyboard)


def create_clock(callback_txt='', data_split=None):
    """
        Create an inline keyboard

    :param callback_txt: текст для формирования ответа кнопки
    :param data_split: содержит параметры и время
    :return: Returns the InlineKeyboardMarkup object with the clock.
    """
    if data_split:
        (action, param, time) = data_split
    else: param, time = None, None
    data_ignore = "IGNORE"
    data_done = "done"
    keyboard = []
    # First row - Hour and Minutes
    hours = [['7', '8', '9', "10", "11"], ['12', '13', '14', "15", "16"], ['17', '18', '19', "20", "21"]]
    minutes = [["10", "20"], ["30", "40"], ["50", "00"]]
    row = []
    row.append(InlineKeyboardButton(' ', callback_data=data_ignore))
    row.append(InlineKeyboardButton('      часы     ', callback_data=data_ignore))
    row.append(InlineKeyboardButton(' ', callback_data=data_ignore))
    row.append(InlineKeyboardButton('мин', callback_data=data_ignore))
    keyboard.append(row)
    # Second row - Time
    for line_h, line_m in zip(hours, minutes):
        row = []
        for h in line_h:
            if param == 'h' and time == h: h = '✓' + h
            row.append(InlineKeyboardButton(h, callback_data=f'{callback_txt};h;{h}'))
        row.append(InlineKeyboardButton(' ', callback_data=data_ignore))
        for m in line_m:
            if param == 'm' and time == m: m = '✓' + m
            row.append(InlineKeyboardButton(m, callback_data=f'{callback_txt};m;{m}'))
        keyboard.append(row)

    row = []
    row.append(InlineKeyboardButton("Отмена", callback_data=data_done))
    keyboard.append(row)

    return InlineKeyboardMarkup(keyboard)

async def process_calendar_selection(update, context):
    """
    Process the callback_query. This method generates a new calendar if forward or
    backward is pressed. This method should be called inside a CallbackQueryHandler.
    :param telegram.Bot bot: The bot, as provided by the CallbackQueryHandler
    :param telegram.Update update: The update, as provided by the CallbackQueryHandler
    :return: Returns a tuple (Boolean,datetime.datetime), indicating if a date is selected
                and returning the date if so.
    """
    ret_data = (False, None)
    query = update.callback_query
    if query:
        data_split = separate_callback_data(query.data)
        if len(data_split) == 4:
            (action, year, month, day) = data_split
            curr = datetime(int(year), int(month), 1)
        elif data_split[0] == 'done_create':
            action = "done"
        else: action = "IGNORE"
        if action == "IGNORE":
            # await query.answer()
            await context.bot.answer_callback_query(callback_query_id=query.id)
        elif action == "DAY" or action == "TODAY":
            await context.bot.edit_message_text(text=query.message.text,
                                                chat_id=query.message.chat_id,
                                                message_id=query.message.message_id
                                                )
            ret_data = True, datetime(int(year), int(month), int(day))
        elif action == "PREV-MONTH":
            pre = curr - timedelta(days=1)
            await context.bot.edit_message_text(text=query.message.text,
                                                chat_id=query.message.chat_id,
                                                message_id=query.message.message_id,
                                                reply_markup=create_calendar(int(pre.year), int(pre.month)))
        elif action == "NEXT-MONTH":
            ne = curr + timedelta(days=31)
            await context.bot.edit_message_text(text=query.message.text,
                                                chat_id=query.message.chat_id,
                                                message_id=query.message.message_id,
                                                reply_markup=create_calendar(int(ne.year), int(ne.month)))
        elif action == "done":
            await context.bot.edit_message_text(text=query.message.text,
                                                chat_id=query.message.chat_id,
                                                message_id=query.message.message_id,
                                                )
            ret_data = False, 'done'
        else:
            await context.bot.answer_callback_query(callback_query_id=query.id, text="Something went wrong!")
            # UNKNOWN
    return ret_data


async def process_clock_selection(update, context, callback_txt=''):
    """
        функция обработки нажатия кнопок времени
    :param callback_txt:
    :return:
    """
    query = update.callback_query
    data_split = separate_callback_data(query.data)
    if len(data_split) == 3:
        (action, param, time) = data_split
        reply_markup = create_clock(callback_txt=callback_txt, data_split=data_split)
    elif data_split[0] == 'done_create':
        action = "done"
    else: action = "IGNORE"
    if action == "IGNORE":
        await context.bot.answer_callback_query(callback_query_id=query.id)
    elif param == "h" or param == "m":
        if reply_markup:
            await context.bot.edit_message_text(text=query.message.text,
                                                chat_id=query.message.chat_id,
                                                message_id=query.message.message_id,
                                                reply_markup=reply_markup
                                                )
        else:
            await context.bot.answer_callback_query(callback_query_id=query.id)
        ret_data = param, time
    elif action == "done":
        await context.bot.edit_message_text(text=query.message.text,
                                            chat_id=query.message.chat_id,
                                            message_id=query.message.message_id
                                            )
        ret_data = False, 'done'
    else:
        await context.bot.answer_callback_query(callback_query_id=query.id, text="Something went wrong!")
        # UNKNOWN
    return ret_data