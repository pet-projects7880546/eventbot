# from pony.orm import Database, Required, Json
from pony.orm import db_session, delete, select
from telegram import InlineKeyboardButton, InlineKeyboardMarkup

from settings import DB_CONFIG, month_to_digit
from models import (Registration,
                    ConversationTable,
                    EventsData,
                    EventCreate,
                    OrganizerTable,
                    LocationTable,
                    EventEdit,
                    ConferenceCreate)
from datetime import datetime
import json

# import calendar

def data_parse(date_text: str):
    """

    :param date_text: разбиение строки вида '15 августа 2023 в 15:00' с датой и временем на отдельные элементы
    :return: tuple
    """
    date_split = date_text.split()
    date = f'{date_split[0]}{month_to_digit[date_split[1]]}{date_split[2]}'
    h, m = date_split[4].split(':')
    return (date, h, m)

def edit_button_menu(num_event):
    """
        функция генерирует кнопки с номером события, которое необходимо редактировать
    :param num_event: строка содержащая номер
    :return:
    """
    button_list = [[InlineKeyboardButton("Название", callback_data=f'edit_name_{num_event}'),
                    InlineKeyboardButton("Организатор", callback_data=f'edit_organizer_{num_event}')],
                   [InlineKeyboardButton("Место", callback_data=f'edit_location_{num_event}'),
                    InlineKeyboardButton("Дата", callback_data=f'edit_date_{num_event}')],
                   [InlineKeyboardButton("Время", callback_data=f'edit_time_{num_event}'),
                    InlineKeyboardButton("Формат", callback_data=f'edit_format_{num_event}')],
                   [InlineKeyboardButton("Ссылка", callback_data=f'edit_link_{num_event}'),
                    InlineKeyboardButton("Комментарий", callback_data=f'edit_comment_{num_event}')]]
                   # [InlineKeyboardButton("Ничего", callback_data='cancel_edit')]]
    return button_list

def replace_text_to_parse(text, param):
    """
        функция для укорачивания названий организаторов и мест проведения
    :param text:
    :param param:
    :return:
    """
    if param == 'organizer':
        patern_organizer = {
            'Астраханская область': 'АО',
            'Астраханской области': 'АО',
            'Государственное казенное учреждение': 'ГКУ',
            'Государственное казённое учреждение': 'ГКУ',
            'Государственное автономное учреждение': 'ГАУ',
            'Министерство государственного управления, информационных технологий и связи': 'МинГос',
        }
        for key, val in patern_organizer.items():
            text = text.replace(key, val)

    elif param == 'location':
        patern_location = {
            'Кабинет руководителя': 'Каб.рук.',
            'Кабинет заместителя председателя Правительства Астраханской области': 'Кабинет зам преда',
            'Кабинет министра государственного управления, информационных технологий и связи Астраханской области': 'МинГос каб. Набутовского',
            'Кабинет заместителя министра государственного управления, информационных технологий и связи Астраханской области (Ядыкин Роман Юрьевич)': 'МинГос каб. Ядыкина',
        }
        for key, val in patern_location.items():
            text = text.replace(key, val)
    return text


def build_menu(buttons, n_cols, header_buttons=None, footer_buttons=None):
    """
        Подготовка многорядного и многострочного блока кнопок
    :param buttons: Список кнопок
    :param n_cols: количество колонок
    :param header_buttons: для указания первого элемента в блоке
    :param footer_buttons: для указания последнего элемента в блоке
    :return:
    """
    menu = [buttons[i:i + n_cols] for i in range(0, len(buttons), n_cols)]
    if header_buttons:
        menu.insert(0, [header_buttons])
    if footer_buttons:
        menu.append([footer_buttons])
    return menu
                                    # 20
def build_button_inline(table_rows, num, callback_txt): # create_organizer edit_organizer  organizer_update
    """
        функция генерирует кнопки с организаторами или местами проведения с возможностью перелистывания
    :param table_rows: таблица со списком организаторов или мест и их параметрами
    :param num: номер с которого нужно начать выводить список (нужно для кнопок пролистывания)
    :param callback_txt:
    :return:
    """
    # сборка клавиатуры из кнопок `InlineKeyboardButton`
    row = []
    keyboard = []
                        # 23            20
    num_end = -1 if len(table_rows) < num + 10 else num + 10 # 20
    for data in table_rows[num:num_end]:
        keyboard.append([InlineKeyboardButton(data.name, callback_data=f'{callback_txt}_{data.position_id}')])#
        # keyboard.append(row)

    row = []
    # callback_txt_split = callback_txt.split('_')[-1]
    if num != 0:                                                #organizer
        row.append(InlineKeyboardButton("<<", callback_data=f"update_{callback_txt}_{num - 10}"))
    else:
        row.append(InlineKeyboardButton(" ", callback_data=f"update_{callback_txt}_ignore"))
    if len(table_rows) < num + 10:
        row.append(InlineKeyboardButton(" ", callback_data=f"update_{callback_txt}_ignore"))
    else:
        row.append(InlineKeyboardButton(">>", callback_data=f"update_{callback_txt}_{num + 10}"))
    keyboard.append(row)

    return InlineKeyboardMarkup(keyboard)

@db_session
def db_get_event(event_id):
    """
    получение из базы предварительно спарсенной информации о мероприятии по идентификатору
    """
    return EventsData.get(event_id=int(event_id))

@db_session
def db_get_user(chat_id=None, role=None):
    """
    получение из базы информации о пользователе бота
    """
    if chat_id: return Registration.get(chat_id=str(chat_id))
    if role: return select(row for row in Registration if row.role == role)[:]

@db_session
def db_registration(db_data, role):
    """
    сохранение пользователя в БД
    """
    Registration(chat_id=str(db_data.id), name=db_data.effective_name,
                     username=db_data.username, role='user')

@db_session
def db_set_useradmin(chat_id, role):
    """
    изменение роли пользователя
    """
    user = Registration.get(chat_id=str(chat_id))
    user.role = role

@db_session
def db_events_data(events):
    """
    запись в базу спарсенной информации о мероприятии
    :param events: список словарей с данными
    :return:
    """
    if events:
        for event in events:
            db_event = EventsData.get(event_id=int(event['id']))
            if db_event is None:
                EventsData(event_id=int(event["id"]),    name=event["data"]["name"],
                           timesec=0,                           organizer=event["data"]["organizer"],
                           location=event["data"]["location"],  time=event["data"]["time"],
                           format=event["data"]["format"],      vcs=event["data"]["vcs"],
                           comment=event["data"]["comment"])
            else:
                db_event.timesec = 0
                db_event.name = event["data"]["name"]
                db_event.organizer = event["data"]["organizer"]
                db_event.location = event["data"]["location"]
                db_event.time = event["data"]["time"]
                db_event.format = event["data"]["format"]
                db_event.vcs = event["data"]["vcs"]
                db_event.comment = event["data"]["comment"]

@db_session
def db_create_event(chat_id, name=None, organizer=None, location=None, location_custom=None, date=None, hour=None, minute=None,
                    format_=None, platform=None, link=None, comment=None, message_id=0):
    """
        функция записывает значения каждого шага при прохождении сценария создания события
    """
    db_event = EventCreate.get(chat_id=str(chat_id))
    if db_event is None:
        EventCreate(chat_id=str(chat_id), message_id=int(message_id))
    else:

        if name is not None: db_event.name = name
        if organizer is not None: db_event.organizer = organizer
        if location is not None: db_event.location = location
        if location_custom is not None: db_event.location_custom = location_custom
        if date is not None: db_event.date = date.strftime("%d.%m.%Y")
        if hour is not None: db_event.hour = hour
        if minute is not None: db_event.minute = minute
        if format_ is not None: db_event.format = format_
        if platform is not None: db_event.platform = platform
        if link is not None: db_event.link = link
        if comment is not None: db_event.comment = comment
        if message_id: db_event.message_id = int(message_id)

@db_session
def db_edit_event(chat_id, event_id = None, status = None, name = None, organizer = None, location = None,
                  location_custom = None, date = None, hour = None, minute = None,
                  format_ = None, platform = None, link = None, comment = None, message_id = 0):
    """
        функция записывает значения каждого шага при прохождении сценария редактирования события и помимо этого
        заполняется значениями, спарсенными с сайта
    """
    # db_event = select(row for row in EventEdit if row.chat_id == str(chat_id))[:]
    db_event = EventEdit.get(chat_id=str(chat_id))
    if db_event is None:
        EventEdit(chat_id=str(chat_id), event_id=str(event_id))
    else:
        if status is not None: db_event.status = status
        if name is not None: db_event.name = name
        if organizer is not None: db_event.organizer = organizer
        if location is not None: db_event.location = location
        if location_custom is not None: db_event.location_custom = location_custom
        if date is not None: db_event.date = date
        if hour is not None: db_event.hour = hour
        if minute is not None: db_event.minute = minute
        if format_ is not None: db_event.format = format_
        if platform is not None: db_event.platform = platform
        if link is not None: db_event.link = link
        if comment is not None: db_event.comment = comment
        if message_id: db_event.message_id = int(message_id)

@db_session
def db_delete_event(chat_id):
    """
        функция очищает запись из таблицы с данными о создании события, после того как эти данные были отправлены
        на сайт для создания
    """
    row = EventCreate.get(chat_id=str(chat_id))
    if row:
        row.delete()

@db_session
def db_get_create_event(chat_id):
    """
        функция получает данные о создании события из таблицы
    """
    return EventCreate.get(chat_id=str(chat_id))

@db_session
def db_get_edit_event(chat_id):
    """
        функция получает данные о редактировании события из таблицы
    """
    return EventEdit.get(chat_id=str(chat_id))

@db_session
def db_clear_eventconversation(chat_id):
    """
        функция очищает записи в таблице, которая хранит информацию о текущем шаге прохождения сценария
    """
    key = str([chat_id, chat_id])
    db_events = select(row for row in ConversationTable if row.key == key)[:]
    for event in db_events:
        event.state = 'None'

@db_session
def db_add_organizer(data):
    """
        функция добавляет в базу спарсенных организаторов с сайта
    """
    orgazizers = select(row for row in OrganizerTable)[:]
    if orgazizers:
        for position_id, name, name_full in data:
            for org in orgazizers:
                if position_id == org.position_id:
                    org.name = name
                    org.name_full = name_full
                    break
                else:
                    continue
            else:
                OrganizerTable(position_id=position_id, name=name, name_full=name_full)
    else:
        for position_id, name, name_full in data:
            OrganizerTable(position_id=position_id, name=name, name_full=name_full)

@db_session
def db_add_location(data):
    """
        функция добавляет в базу спарсенные локации с сайта
    """
    locations = select(row for row in LocationTable)[:]
    if locations:
        for position_id, name, name_full in data:
            for loc in locations:
                if position_id == loc.position_id:
                    loc.name = name
                    loc.name_full = name_full
                    break
                else:
                    continue
            else:
                LocationTable(position_id=position_id, name=name, name_full=name_full)
    else:
        for position_id, name, name_full in data:
            LocationTable(position_id=position_id, name=name, name_full=name_full)

@db_session
def db_get_location():
    """
        функция получает список локаций из базы
    """
    return select(row for row in LocationTable).order_by(LocationTable.priority)[:]

@db_session
def db_get_organizer():
    """
        функция получает список организаторов из базы
    """
    return select(row for row in OrganizerTable).order_by(OrganizerTable.priority)[:]

@db_session
def db_get_organizer_position_id(name_full):
    """
        функция получает из базы организатора по полному названию, взятому с сайта
    """
    return select(row for row in OrganizerTable if row.name_full == name_full)[:]

@db_session
def db_get_location_position_id(name_full):
    """
        функция получает из базы локацию по полному названию, взятому с сайта
    """
    return select(row for row in LocationTable if row.name_full == name_full)[:]

@db_session
def db_delete_edit_event(chat_id):
    """
        функция очищает запись из таблицы с данными о редактировании события, после того как эти данные были отправлены
        на сайт для создания
    """
    row = EventEdit.get(chat_id=str(chat_id))
    if row:
        row.delete()

@db_session
def db_create_link(chat_id, name=None, owner=None, type_conf=None, max_podiums=None):
    """
        функция записывает в базу значения каждого шага при прохождении сценария создания ссылки на trueconf сервере
    """
    db_conf = ConferenceCreate.get(chat_id=str(chat_id))
    if db_conf is None:
        ConferenceCreate(chat_id=str(chat_id))
    else:
        if name is not None: db_conf.name = name
        if owner is not None: db_conf.owner = owner
        if type_conf is not None: db_conf.type_conf = type_conf
        if max_podiums is not None: db_conf.max_podiums = max_podiums

@db_session
def db_get_create_link(chat_id):
    """
        функция получает из базы значения для создание ссылки, которые отправляются на сервер
    """
    return ConferenceCreate.get(chat_id=str(chat_id))

@db_session
def db_delete_create_link(chat_id):
    """
        функция очищает запись из таблицы с данными о создании ссылки, после того как эти данные были отправлены
        на сайт для создания
    """
    row = ConferenceCreate.get(chat_id=str(chat_id))
    if row:
        row.delete()

@db_session
def db_get_conversation(chat_id):
    """
        функция получает данные из таблицы, в которой храняться текущие шаги прохождения сценария
    """
    key = str([chat_id, chat_id])
    return select(row for row in ConversationTable if row.key == key)[:]
