#pip install -r requirements.txt


anyio==3.7.1
attrs==23.1.0
beautifulsoup4==4.12.2
bs4==0.0.1
certifi==2023.5.7
cffi==1.15.1
charset-normalizer==3.2.0
exceptiongroup==1.1.2
h11==0.14.0
httpcore==0.17.3
httpx==0.24.1
idna==3.4
outcome==1.2.0
Pillow==10.0.0
pony==0.7.16
psycopg2==2.9.6
pycparser==2.21
PySocks==1.7.1
python-dateutil==2.8.2
python-telegram-bot==20.4
#python-telegram-bot-calendar==1.0.5
requests==2.31.0
selenium==4.10.0
six==1.16.0
sniffio==1.3.0
sortedcontainers==2.4.0
soupsieve==2.4.1
trio==0.22.2
trio-websocket==0.10.3
urllib3==2.0.4
wsproto==1.2.0
