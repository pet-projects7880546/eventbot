from pony.orm import Database, Required, Optional, Json
from pony.orm import db_session

from settings import DB_CONFIG

db = Database()
db.bind(**DB_CONFIG)

class Registration(db.Entity):
    """Старт бота. регистрация"""
    chat_id = Required(str, unique=True)
    name = Required(str)
    username = Required(str)
    role = Required(str)


class EventsData(db.Entity):
    """Сохранение сообщений пользователя"""
    event_id = Required(int, unique=True)
    name = Required(str)
    timesec = Optional(float)
    organizer = Required(str)
    location = Required(str)
    time = Required(str)
    format = Required(str)
    vcs = Optional(str, )
    comment = Optional(str)

class EventCreate(db.Entity):
    """Сохранение шагов создания события"""
    chat_id = Required(str, unique=True)
    name = Optional(str)
    organizer = Optional(str)
    location = Optional(str)
    location_custom = Optional(str)
    date = Optional(str)
    hour = Optional(str)
    minute = Optional(str)
    format = Optional(str)
    platform = Optional(str)
    link = Optional(str)
    comment = Optional(str)
    message_id = Optional(int)

class EventEdit(db.Entity):
    """Сохранение изменений в событии"""
    chat_id = Required(str, unique=True)
    event_id = Optional(str)
    status = Optional(str)
    name = Optional(str)
    organizer = Optional(str)
    location = Optional(str)
    location_custom = Optional(str)
    date = Optional(str)
    hour = Optional(str)
    minute = Optional(str)
    format = Optional(str)
    platform = Optional(str)
    link = Optional(str)
    comment = Optional(str)
    message_id = Optional(int)


class ConversationTable(db.Entity):
    """Сохранение состояния сценария"""
    name = Required(str)
    key = Required(str)
    state = Required(str)

class OrganizerTable(db.Entity):
    """Сохранение всех имеющихся организаторов"""
    position_id = Required(str)
    name = Required(str)
    name_full = Required(str)
    priority = Optional(str)

class LocationTable(db.Entity):
    """Сохранение всех мест проведения"""
    position_id = Required(str)
    name = Required(str)
    name_full = Required(str)
    priority = Optional(str)

class ConferenceCreate(db.Entity):
    """Сохранение данных для создания ссылки"""
    chat_id = Required(str, unique=True)
    name = Optional(str)
    owner = Optional(str)
    type_conf = Optional(int)
    max_podiums = Optional(int)

db.generate_mapping(create_tables=True)