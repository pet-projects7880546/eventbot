import json
from copy import deepcopy
from models import ConversationTable
from telegram.ext import BasePersistence, PersistenceInput
from telegram.ext._utils.types import UD, CD, BD, CDCData, ConversationDict, ConversationKey
import typing as t
from pony.orm import db_session, Database, Required, Json, select



class PonyPersistence(BasePersistence[UD, CD, BD]):

    """Кастомный класс TelegramBotApi для сохранения прохождения сценария пользователями в БД используя ponyORM"""

    def __init__(self):
        self.store_data = PersistenceInput(chat_data=False, bot_data=False, user_data=False, callback_data=False)
        super().__init__(store_data=self.store_data, update_interval=60)

        self.chat_data: Optional[Dict[int, CD]] = None
        self.conversations: Optional[Dict[str, Dict[Tuple[Union[int, str], ...], object]]] = None

    async def get_chat_data(self) -> t.DefaultDict[int, t.Any]:
        return {}

    async def update_chat_data(self, chat_id: int, data: CD) -> None:
        pass

    async def refresh_chat_data(self, chat_id: int, chat_data: t.Any) -> None:
        pass

    async def drop_chat_data(self, chat_id: int) -> None:
        pass

    async def get_bot_data(self) -> t.Any:
        pass

    def update_bot_data(self, data) -> None:
        pass

    def refresh_bot_data(self, bot_data) -> None:
        pass

    def get_user_data(self) -> t.DefaultDict[int, t.Any]:
        pass

    def update_user_data(self, user_id: int, data: t.Any) -> None:
        pass

    def refresh_user_data(self, user_id: int, user_data: t.Any) -> None:
        pass

    def get_callback_data(self) -> t.Optional[t.Any]:
        pass

    def update_callback_data(self, data: t.Any) -> None:
        pass

    @db_session
    def db_get_conversations(self, name: str):
        return select(row for row in ConversationTable if row.name == name)[:]

    async def get_conversations(self, name: str) -> ConversationDict:
        '''Returns the conversations from the pickle on Database if it exsists or an empty dict.'''
        cov_users = {}
        if self.conversations and name in self.conversations:
            pass
        else:
            conv_data = self.db_get_conversations(name=name)
            if not conv_data:
                self.conversations = {name: {}}
            else:
                for conv in conv_data:
                    state = None if conv.state == "None" else conv.state
                    cov_users[tuple(json.loads(conv.key))] = state #словарь из пар {(чатid, чатid): состояние, ...}
                self.conversations = {name: cov_users}
        return self.conversations.get(name, {}).copy()  # type: ignore[union-attr]

    @db_session
    def db_update_conversation(self, name: str, key, state):
        conv = select(row for row in ConversationTable if row.name == name and row.key == json.dumps(key))[:] # and row.state == state
        state = "None" if state is None else state
        if conv:
            conv[0].state = state
        else:
            ConversationTable(name=name, key=json.dumps(key), state=state)

    async def update_conversation(self, name: str, key, new_state: t.Optional[object]) -> None:
        self.db_update_conversation(name=name, key=key, state=new_state)

    async def flush(self) -> None:
        pass

    async def drop_user_data(self, user_id: int) -> None:
        pass

    async def get_user_data(self) -> t.Dict[int, UD]:
        pass
